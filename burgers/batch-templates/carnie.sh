#!/bin/bash

{% for option in options %}#SBATCH --{{ option.name }}={{ option.value }}
{% endfor %}
#Load required modules

module load julia 

# Run Julia
# julia -t n_threads impmid.jl FullType ReduType nt nx

julia -t {{ cmd.n_threads }} \
	{{ cmd.filename }} \
	{{ cmd.fulltype }} \
	{{ cmd.redutype }} \
	$((10 ** {{ cmd.nt }} )) \
	{{ cmd.nx }} {% if cmd.is_corr %}\
	{{ cmd.ncorr }}
	{% endif %}