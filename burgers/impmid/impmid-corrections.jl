
using Quadmath;
#using NLsolve;
#using BenchmarkTools;
using LinearAlgebra;
using TimerOutputs;

##########################################
# Usage
#
# julia impmid-corrections.jl FullType ReduType nt nx ncorr
# Example
# julia impmid-corrections.jl Float64 Float32 100000 20 1
#
##########################################

#BLAS threading
BLAS.set_num_threads(Threads.nthreads())

const FullFloat = eval(Meta.parse(ARGS[1]));
const ReduFloat = eval(Meta.parse(ARGS[2]));

############ Functions

function burgers_imr_F!(n::Integer, β::ReduFloat, dx::ReduFloat, dt::ReduFloat, u::Array{ReduFloat}, F::Array{ReduFloat}, y::Array{ReduFloat})
    F[1] = ReduFloat(0.0)
    F[n] = ReduFloat(0.0)
    for i=2:n-1
        #implicit burgers step F = 0 for y = u + dt/2 * f(y)
        # F = u - y + dt/2 * (βYxx - 1/2(Y^2)x)
        F[i] = (u[i] - y[i] 
            + dt*ReduFloat(0.5) * (
            β/(dx*dx) * (y[i+1] + y[i-1] - ReduFloat(2.0) * y[i]) 
            - ReduFloat(0.5) * (y[i+1]*y[i+1] - y[i-1]*y[i-1]) / (ReduFloat(2.0)*dx))
            )    
    end
end

function burgers_imr_J!(n::Integer, β::ReduFloat, dx::ReduFloat, dt::ReduFloat, J::Matrix{ReduFloat}, y::Array{ReduFloat})
    J[:, :] .= ReduFloat(0.0)

    for i=1:n
        #Jacobian for the implicit burgers step F=0 for y = u + dt/2 * f(y)
        # F = u - y + dt/2 * (βYxx - 1/2(Y^2)x)
        J[i, i] = ReduFloat(-1.0) - dt*ReduFloat(0.5) * β * ReduFloat(2.0) / (dx*dx) 
        if i-1 != 0
            J[i, i-1] = dt*ReduFloat(0.5) * (β / (dx*dx) + y[i-1]/(ReduFloat(2.0) * dx))
        end
        if i+1 != n+1 
            J[i, i+1] = dt*ReduFloat(0.5) * (β / (dx*dx) - y[i+1]/(ReduFloat(2.0) * dx))
        end
    end
end

#=
My newtons method. Takes in inital guess x0 and mutates it to the solution over
max_iter iterations. Fn and Jn are preallocated placeholders for the iteration
computations. 
=#
function my_newtons!(f!, j!, x0::Array{ReduFloat}, Fn::Array{ReduFloat}, Jn::Matrix{ReduFloat})::Array{ReduFloat}
    tol = eps(ReduFloat) * .5e1; #Tolerance of the solver slightly larger than machine epsilon for that precision
    max_iter = 20; #maximum number of iterations for the solver 

    #println("starting newtons")
    for i=1:max_iter
        f!(Fn, x0)
        if norm(Fn) < tol 
            #println("iter: $i, tol: $(norm(Fn))")
            return x0
        end

        j!(Jn, x0)

        x0 = x0 - inv(Jn) * Fn;
        #println(norm(Fn))
    end
    return x0
end

function init_u(xs::Array{FullFloat}, n::Integer)::Array{FullFloat}
    #u = zeros(Float64, n)
    #for i=1:n-1
    #    u[i] = sin(2.0 * pi * xs[i])
    #end
    #Equivalent to
    u = sin.(FullFloat(2.0) * pi * xs)
    u[n] = 0.0 #make sure end boundary is 0.0
    return u
end

function impmid_driver(nt::Integer, tf::FullFloat, nx::Integer, beta::FullFloat, ts::Array{FullFloat}, xs::Array{FullFloat}, ncorr::Integer)::Array{FullFloat}
    dt = ts[2] - ts[1]
    dx = xs[2] - xs[1]
    t_total = FullFloat(ts[1])
    u = init_u(xs, nx)
    unp1 = copy(u)
    fn = zeros(ReduFloat, nx)
    jn = zeros(ReduFloat, nx, nx)
    unp1 = copy(u)
    #jacobian doesn't change each time step so leave it out of the loop
    j!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = burgers_imr_J!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt), J, y)

    for (i, t) in enumerate(ts)
        #Make sure final time is reached correctly
        if i == nt
            dt = tf - t_total
        end
        #implicit step
        tmp_u = convert(Array{ReduFloat}, u)
        #implicit function changes with u, so redefine each time step
        f!(F::Array{ReduFloat}, y::Array{ReduFloat}) = burgers_imr_F!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt), tmp_u, F, y)
        #y1 = nlsolve(f!, j!, tmp_u).zero #solve the nonlinear system f! using initial guess u
        y1 = my_newtons!(f!, j!, tmp_u, fn, jn)

        #correction step
        yk = convert(Array{FullFloat}, y1)
        for p=1:ncorr
            for k=2:nx-1
                yk[k] = (u[k] 
                    + dt*beta/(FullFloat(2.0)*dx*dx) * (yk[k-1] + yk[k+1] - FullFloat(2.0) * yk[k]) 
                    - (dt / (FullFloat(8.0) * dx) * (yk[k+1]*yk[k+1] - yk[k-1]*yk[k-1]))
                    )
            end
        end

        #explicit step
        unp1[1] = u[1]
        for j=2:nx-1
            unp1[j] = (u[j] 
                + dt*beta/(dx*dx) * (yk[j-1] + yk[j+1] - FullFloat(2.0) * yk[j]) 
                - (dt / (FullFloat(4.0) * dx) * (yk[j+1]*yk[j+1] - yk[j-1]*yk[j-1]))
                )
        end
        unp1[nx] = u[nx]
        
        #update
        u = unp1
        t_total += dt
    end
    
    return u
end

######### Main

#initial conditions
beta = FullFloat(1e-2);
ti = FullFloat(0.0);
tf = FullFloat(1.0);
xi = FullFloat(0.0);
xf = FullFloat(1.0);

nt = parse(Int, ARGS[3]);
nx = parse(Int, ARGS[4]);
ncorr = parse(Int, ARGS[5]);

ts = collect(range(ti, tf, nt+1)[1:nt]);
xs = collect(range(xi, xf, nx));

#BenchmarkTools.DEFAULT_PARAMETERS.samples = 4;
#@btime impmid_driver(nt, tf, nx, beta, ts, xs)

#=
This takes a long time to run for Float128/BigFloat for large nt.
To make sure everything gets done in a reasonable amount of time,
we run once and if that time is greater than 10min we assume that
no averaging is needed on runtime. At this point we can safely 
ignore system noise and know that it is the method taking up all
the time. Unfortunately in Julia this means the inclusion of compile
time, but I think it is safe to assume that for times greater than
this threshold compile time can be treated as noise.
=#
elapsed_time = @elapsed begin
    u = impmid_driver(nt, tf, nx, beta, ts, xs, ncorr)
end;

if elapsed_time < 60 * 10 # 10min  
    global tot_elapsed = 0.0
    nruns = 4
    for k=1:nruns
        elapsed_time_ave = @elapsed begin
            uel = impmid_driver(nt, tf, nx, beta, ts, xs, ncorr)
        end;
        #println("One done in: $elapsed_time")
        global tot_elapsed = tot_elapsed + elapsed_time_ave
    end
    tot_elapsed = tot_elapsed / nruns
    println("Ave elapsed time: $tot_elapsed")
else 
    println("Ave elapsed time: $elapsed_time")
end

#verify the error in quad precision (on x86_64)
u_ref = zeros(Float128, nx)
open("./ref_sol/ref_sol_$nx.txt") do f 
    for (i, line) in enumerate(eachline(f))
        u_ref[i] = parse(Float128, line)
    end
end

dx = Float128(xs[2] - xs[1])
tmp = convert(Array{Float128}, u) - u_ref
l2_err = sqrt.(dx * dot(tmp, tmp))
println("L2 error with reference: $l2_err")
