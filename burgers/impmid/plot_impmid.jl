
using Quadmath;
using NLsolve;
#using BenchmarkTools;
using LinearAlgebra;
using TimerOutputs;
using Plots;

##########################################
# Usage
#
# julia impmid.jl FullType ReduType nt nx 
# Example
# julia impmid.jl Float64 Float32 100000 20
#
##########################################

const FullFloat = eval(Meta.parse(ARGS[1]));
const ReduFloat = eval(Meta.parse(ARGS[2]));

############ Functions

function burgers_imr_F!(n::Integer, β::ReduFloat, dx::ReduFloat, dt::ReduFloat, u::Array{ReduFloat}, F::Array{ReduFloat}, y::Array{ReduFloat})
    F[1] = ReduFloat(0.0)
    F[n] = ReduFloat(0.0)
    for i=2:n-1
        #implicit burgers step F = 0 for y = u + dt/2 * f(y)
        # F = u - y + dt/2 * (βYxx - 1/2(Y^2)x)
        F[i] = (u[i] - y[i] 
            + dt*ReduFloat(0.5) * (
            β/(dx*dx) * (y[i+1] + y[i-1] - ReduFloat(2.0) * y[i]) 
            - ReduFloat(0.5) * (y[i+1]*y[i+1] - y[i-1]*y[i-1]) / (ReduFloat(2.0)*dx))
            )    
    end
end

function burgers_imr_J!(n::Integer, β::ReduFloat, dx::ReduFloat, dt::ReduFloat, J::Matrix{ReduFloat}, y::Array{ReduFloat})
    J[:, :] .= ReduFloat(0.0)

    for i=1:n
        #Jacobian for the implicit burgers step F=0 for y = u + dt/2 * f(y)
        # F = u - y + dt/2 * (βYxx - 1/2(Y^2)x)
        J[i, i] = ReduFloat(-1.0) - dt*ReduFloat(0.5) * β * ReduFloat(2.0) / (dx*dx) 
        if i-1 != 0
            J[i, i-1] = dt*ReduFloat(0.5) * (β / (dx*dx) + y[i-1]/(ReduFloat(2.0) * dx))
        end
        if i+1 != n+1 
            J[i, i+1] = dt*ReduFloat(0.5) * (β / (dx*dx) - y[i+1]/(ReduFloat(2.0) * dx))
        end
    end
end

#=
My newtons method. Takes in inital guess x0 and mutates it to the solution over
max_iter iterations. Fn and Jn are preallocated placeholders for the iteration
computations. 
=#
function my_newtons!(f!, j!, x0::Array{ReduFloat}, Fn::Array{ReduFloat}, Jn::Matrix{ReduFloat})
    tol = eps(ReduFloat) * 1e3; #Tolerance of the solver is two orders of magnitude larger than machine epsilon for that precision
    max_iter = 20; #maximum number of iterations for the solver 

    #println("starting newtons")
    for i=1:max_iter
        f!(Fn, x0)
        if norm(Fn) < tol 
            #println("iter: $i, tol: $(norm(Fn))")
            return x0
        end

        j!(Jn, x0)

        x0 = x0 - inv(Jn) * Fn;
        #println(norm(Fn))
    end
    return x0
end

function init_u(xs::Array{FullFloat}, n::Integer)::Array{FullFloat}
    #u = zeros(Float64, n)
    #for i=1:n-1
    #    u[i] = sin(2.0 * pi * xs[i])
    #end
    #Equivalent to
    u = sin.(FullFloat(2.0) * pi * xs)
    u[n] = 0.0 #make sure end boundary is 0.0
    return u
end

function impmid_driver(nt::Integer, nx::Integer, beta::FullFloat, ts::Array{FullFloat}, xs::Array{FullFloat})::Array{FullFloat}
    dt = ts[2] - ts[1]
    dx = xs[2] - xs[1]
    u = init_u(xs, nx)
    unp1 = copy(u)
    fn = zeros(ReduFloat, nx)
    jn = zeros(ReduFloat, nx, nx)
    #jacobian doesn't change each time step so leave it out of the loop
    j!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = burgers_imr_J!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt), J, y)

    nframes = 50
    frame_every = nt ÷ nframes
    anim = Animation()
    plt = plot(xs, u, lim=(-1.0, 1.0), xlim=(xs[1], xs[nx]), title="t = 0")
    
    for (i, t) in enumerate(ts)
        #implicit step
        tmp_u = convert(Array{ReduFloat}, u)
        #implicit function changes with u, so redefine each time step
        f!(F::Array{ReduFloat}, y::Array{ReduFloat}) = burgers_imr_F!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt), tmp_u, F, y)
        #y1 = nlsolve(f!, j!, tmp_u).zero #solve the nonlinear system f! using initial guess u
        y1 = my_newtons!(f!, j!, tmp_u)

        #explicit step
        y1f = convert(Array{FullFloat}, tmp_u)
        unp1[1] = u[1]
        for j=2:nx-1
            unp1[j] = (u[j] 
                + dt*beta/(dx*dx) * (y1f[j-1] + y1f[j+1] - FullFloat(2.0) * y1f[j]) 
                - (dt / (FullFloat(4.0) * dx) * (y1f[j+1]*y1f[j+1] - y1f[j-1]*y1f[j-1]))
                )
        end
        unp1[nx] = u[nx]
        
        #update and plot
        u = unp1
        if mod(i, frame_every) == 0
            plt = plot(xs, u, lim=(-1.0, 1.0), xlim=(xs[1], xs[nx]), title="t = $t")
            frame(anim, plt)
        end
    end
    
    gif(anim, "./burgers_myimr_nt$(nt)_nx$(nx).gif", fps=10)
    return u
end

######### Main

#initial conditions
beta = FullFloat(1e-2);
ti = FullFloat(0.0);
tf = FullFloat(1.0);
xi = FullFloat(0.0);
xf = FullFloat(1.0);

nt = parse(Int, ARGS[3]);
nx = parse(Int, ARGS[4]);

ts = collect(range(ti, tf, nt+1)[1:nt]);
xs = collect(range(xi, xf, nx));

#BenchmarkTools.DEFAULT_PARAMETERS.samples = 4;
#@btime impmid_driver(nt, nx, beta, ts, xs)

#u = impmid_driver(nt, nx, beta, ts, xs);

#=
global tot_elapsed = 0.0
nruns = 5
for k=1:nruns
    elapsed_time = @elapsed begin
        u = impmid_driver(nt, nx, beta, ts, xs)
    end;
    global tot_elapsed = tot_elapsed + elapsed_time 
end
tot_elapsed = tot_elapsed / nruns
println("Ave elapsed time: $tot_elapsed")
=#
elapsed_time = @elapsed begin 
	u = impmid_driver(nt, nx, beta, ts, xs)
end;

println("method time: $elapsed_time")

u_ref = zeros(Float128, nx)
open("./ref_sol/ref_sol_$nx.txt") do f 
    for (i, line) in enumerate(eachline(f))
        u_ref[i] = parse(Float128, line)
    end
end

dx = Float128(xs[2] - xs[1])
tmp = convert(Array{Float128}, u) - u_ref
l2_err = sqrt.(dx * dot(tmp, tmp))
println("L2 error with reference: $l2_err")
