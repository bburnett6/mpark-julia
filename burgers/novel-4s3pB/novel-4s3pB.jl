
using Quadmath;
#using NLsolve;
#using BenchmarkTools;
using LinearAlgebra;
using TimerOutputs;

##########################################
# Usage
#
# julia -t n_threads novel-4s3pA.jl FullType ReduType nt nx 
# Example
# julia -t 4 novel-4s3pA.jl Float64 Float32 100000 20
#
##########################################

#BLAS threading
BLAS.set_num_threads(Threads.nthreads())

const FullFloat = eval(Meta.parse(ARGS[1]));
const ReduFloat = eval(Meta.parse(ARGS[2]));

############ Functions

function burgers_novelA_F!(n::Integer, β::ReduFloat, dx::ReduFloat, dt::ReduFloat, u::Array{ReduFloat}, F::Array{ReduFloat}, y::Array{ReduFloat})
    F[1] = ReduFloat(0.0)
    F[n] = ReduFloat(0.0)
    for i=2:n-1
        #implicit burgers step F = 0 for y = u + dt * f(y)
        # F = u - y + dt * (βYxx - 1/2(Y^2)x)
        F[i] = (u[i] - y[i] 
            + dt * (
                (β/(dx*dx)) * (y[i+1] + y[i-1] - ReduFloat(2.0) * y[i]) 
                - (ReduFloat(0.5) / (ReduFloat(2.0)*dx)) * (y[i+1]*y[i+1] - y[i-1]*y[i-1])
                )
            )    
    end
end

function burgers_novelA_J!(n::Integer, β::ReduFloat, dx::ReduFloat, dt::ReduFloat, J::Matrix{ReduFloat}, y::Array{ReduFloat})
    J[:, :] .= ReduFloat(0.0)
    J[1, 1] = ReduFloat(1.0) #Set to 1 for Dirichlet boundary conditions
    J[n, n] = ReduFloat(1.0)
    for i=2:n-1
        #Jacobian for the implicit burgers step F=0 for y = u + dt * f(y)
        # F = u - y + dt * (βYxx - 1/2(Y^2)x)
        J[i, i] = ReduFloat(-1.0) + dt * β * ReduFloat(-2.0) / (dx*dx) 
        J[i, i+1] = dt * ((β/(dx*dx)) - y[i+1]/(ReduFloat(2.0) * dx))
        J[i, i-1] = dt * ((β/(dx*dx)) + y[i-1]/(ReduFloat(2.0) * dx))
    end
end

#=
My newtons method. Takes in inital guess x0 and mutates it to the solution over
max_iter iterations. Fn and Jn are preallocated placeholders for the iteration
computations. 
=#
function my_newtons!(f!, j!, x0::Array{ReduFloat}, Fn::Array{ReduFloat}, Jn::Matrix{ReduFloat})::Array{ReduFloat}
    tol = eps(ReduFloat) * .5e1; #Tolerance of the solver slightly larger than machine epsilon for that precision
    max_iter = 20; #maximum number of iterations for the solver 

    #println("starting newtons")
    for i=1:max_iter
        f!(Fn, x0)
        if norm(Fn) < tol 
            #println("iter: $i, tol: $(norm(Fn))")
            return x0
        end

        j!(Jn, x0)

        #x0 = x0 - inv(Jn) * Fn;
       	x0 = x0 - Jn \ Fn;
        #println(norm(Fn))
    end
    return x0
end

function init_u(xs::Array{FullFloat}, n::Integer)::Array{FullFloat}
    #u = zeros(Float64, n)
    #for i=1:n-1
    #    u[i] = sin(2.0 * pi * xs[i])
    #end
    #Equivalent to
    u = sin.(FullFloat(2.0) * pi * xs)
    u[n] = 0.0 #make sure end boundary is 0.0
    return u
end

function novelA_driver(nt::Integer, tf::FullFloat, nx::Integer, beta::FullFloat, ts::Array{FullFloat}, xs::Array{FullFloat})::Array{FullFloat}
    dt = ts[2] - ts[1]
    dx = xs[2] - xs[1]
    t_total = FullFloat(ts[1])
    u = init_u(xs, nx)
    unp1 = copy(u)
    fn = zeros(ReduFloat, nx)
    jn = zeros(ReduFloat, nx, nx)
    #Butcher coefficient matrix 
    butcherA = zeros(FullFloat, 4, 4)
    butcherA[2, 1] = 2.543016042796356
    butcherA[3, 1] = 2.451484396921318
    butcherA[3, 2] = 0.024108961241221
    butcherA[4, 1] = 2.073861819468268
    butcherA[4, 2] = 2.367724727682735
    butcherA[4, 3] = 1.711868223075524

    butcherAe = zeros(FullFloat, 4, 4)
    butcherAe[1, 1] = 0.5
    butcherAe[2, 1] = -2.376349376129689
    butcherAe[2, 2] = 0.5
    butcherAe[3, 1] = -2.951484396921318
    butcherAe[3, 2] = 0.475891038758779
    butcherAe[3, 3] = 0.5
    butcherAe[4, 1] = -0.573861819468268
    butcherAe[4, 2] = -3.867724727682735
    butcherAe[4, 3] = -1.211868223075524
    butcherAe[4, 4] = 0.5

    butcherB = zeros(FullFloat, 4)
    butcherB[1] = 1.5
    butcherB[2] = -1.5
    butcherB[3] = 0.5
    butcherB[4] = 0.5

    j1!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = burgers_novelA_J!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt * butcherAe[1, 1]), J, y)
    j2!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = burgers_novelA_J!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt * butcherAe[2, 2]), J, y)
    j3!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = burgers_novelA_J!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt * butcherAe[3, 3]), J, y)
    j4!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = burgers_novelA_J!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt * butcherAe[4, 4]), J, y)

    for (i, t) in enumerate(ts)
        #Make sure final time is reached
        if i == nt
            dt = tf - t_total
        end
        #Stage 1 (implicit)
        tmp_u = convert(Array{ReduFloat}, u)
        #implicit function changes with u, so redefine each time step
        f1!(F::Array{ReduFloat}, y::Array{ReduFloat}) = burgers_novelA_F!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt * butcherAe[1, 1]), tmp_u, F, y)
        y1 = my_newtons!(f1!, j1!, tmp_u, fn, jn)
        y1f = FullFloat.(y1)

        #Stage 2 (implicit)
        tmp_u2 = zeros(FullFloat, nx)
        for j=2:nx-1
            tmp_u2[j] = (u[j] +
                (dt * butcherA[2, 1]) * (
                    beta/(dx*dx) * (y1f[j+1] + y1f[j-1] - FullFloat(2.0)*y1f[j])
                    - FullFloat(0.5) * ((y1f[j+1]*y1f[j+1] - y1f[j-1]*y1f[j-1])/(FullFloat(2.0)*dx))
                )
            )
        end
        tmp_u2r = ReduFloat.(tmp_u2)
        for j=2:nx-1
            tmp_u2r[j] += (ReduFloat(dt * butcherAe[2, 1]) * (
                ReduFloat(beta/(dx*dx)) * (y1[j+1] + y1[j-1] - ReduFloat(2.0)*y1[j])
                - ReduFloat(0.5) * ((y1[j+1]*y1[j+1] - y1[j-1]*y1[j-1])/(ReduFloat(2.0*dx)))
                )
            )
        end
        f2!(F::Array{ReduFloat}, y::Array{ReduFloat}) = burgers_novelA_F!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt * butcherAe[2, 2]), tmp_u2r, F, y)
        y2 = my_newtons!(f2!, j2!, tmp_u2r, fn, jn)
        y2f = FullFloat.(y2)

        #Stage 3 (implicit)
        tmp_u3 = zeros(FullFloat, nx)
        for j=2:nx-1
            tmp_u3[j] = (u[j] + 
                (dt * butcherA[3, 1]) * (
                    beta/(dx*dx) * (y1f[j+1] + y1f[j-1] - FullFloat(2.0)*y1f[j])
                    - FullFloat(0.5) * ((y1f[j+1]*y1f[j+1] - y1f[j-1]*y1f[j-1])/(FullFloat(2.0)*dx))
                ) + (dt * butcherA[3, 2]) * (
                    beta/(dx*dx) * (y2f[j+1] + y2f[j-1] - FullFloat(2.0)*y2f[j])
                    - FullFloat(0.5) * ((y2f[j+1]*y2f[j+1] - y2f[j-1]*y2f[j-1])/(FullFloat(2.0)*dx))
                ) 
            )
        end
        tmp_u3r = ReduFloat.(tmp_u3)
        for j = 2:nx-1
            tmp_u3r[j] += (ReduFloat(dt * butcherAe[3, 1]) * (
                    ReduFloat(beta/(dx*dx)) * (y1[j+1] + y1[j-1] - ReduFloat(2.0)*y1[j])
                    - ReduFloat(0.5) * ((y1[j+1]*y1[j+1] - y1[j-1]*y1[j-1])/(ReduFloat(2.0*dx)))
                ) + ReduFloat(dt * butcherAe[3, 2]) * (
                    ReduFloat(beta/(dx*dx)) * (y2[j+1] + y2[j-1] - ReduFloat(2.0)*y2[j])
                    - ReduFloat(0.5) * ((y2[j+1]*y2[j+1] - y2[j-1]*y2[j-1])/(ReduFloat(2.0*dx)))
                )
            )
        end
        f3!(F::Array{ReduFloat}, y::Array{ReduFloat}) = burgers_novelA_F!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt * butcherAe[3, 3]), tmp_u3r, F, y)
        y3 = my_newtons!(f3!, j3!, tmp_u3r, fn, jn)
        y3f = FullFloat.(y3)

        #Stage 4 (implicit)
        tmp_u4 = zeros(FullFloat, nx)
        for j=2:nx-1
            tmp_u4[j] = (u[j] + 
                (dt * butcherA[4, 1]) * (
                    beta/(dx*dx) * (y1f[j+1] + y1f[j-1] - FullFloat(2.0)*y1f[j])
                    - FullFloat(0.5) * ((y1f[j+1]*y1f[j+1] - y1f[j-1]*y1f[j-1])/(FullFloat(2.0)*dx))
                ) + (dt * butcherA[4, 2]) * (
                    beta/(dx*dx) * (y2f[j+1] + y2f[j-1] - FullFloat(2.0)*y2f[j])
                    - FullFloat(0.5) * ((y2f[j+1]*y2f[j+1] - y2f[j-1]*y2f[j-1])/(FullFloat(2.0)*dx))
                ) + (dt * butcherA[4, 3]) * (
                    beta/(dx*dx) * (y3f[j+1] + y3f[j-1] - FullFloat(2.0)*y3f[j])
                    - FullFloat(0.5) * ((y3f[j+1]*y3f[j+1] - y3f[j-1]*y3f[j-1])/(FullFloat(2.0)*dx))
                )
            )
        end
        tmp_u4r = ReduFloat.(tmp_u4)
        for j = 2:nx-1
            tmp_u4r[j] += (ReduFloat(dt * butcherAe[4, 1]) * (
                    ReduFloat(beta/(dx*dx)) * (y1[j+1] + y1[j-1] - ReduFloat(2.0)*y1[j])
                    - ReduFloat(0.5) * ((y1[j+1]*y1[j+1] - y1[j-1]*y1[j-1])/(ReduFloat(2.0*dx)))
                ) + ReduFloat(dt * butcherAe[4, 2]) * (
                    ReduFloat(beta/(dx*dx)) * (y2[j+1] + y2[j-1] - ReduFloat(2.0)*y2[j])
                    - ReduFloat(0.5) * ((y2[j+1]*y2[j+1] - y2[j-1]*y2[j-1])/(ReduFloat(2.0*dx)))
                ) + ReduFloat(dt * butcherAe[4, 3]) * (
                    ReduFloat(beta/(dx*dx)) * (y3[j+1] + y3[j-1] - ReduFloat(2.0)*y3[j])
                    - ReduFloat(0.5) * ((y3[j+1]*y3[j+1] - y3[j-1]*y3[j-1])/(ReduFloat(2.0*dx)))
                )
            )
        end
        f4!(F::Array{ReduFloat}, y::Array{ReduFloat}) = burgers_novelA_F!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt * butcherAe[4, 4]), tmp_u4r, F, y)
        y4 = my_newtons!(f4!, j4!, tmp_u4r, fn, jn)
        y4f = FullFloat.(y4)
        
        #update
        for j=2:nx-1
            unp1[j] = (u[j] + 
                (dt * butcherB[1]) * (
                    beta/(dx*dx) * (y1f[j+1] + y1f[j-1] - FullFloat(2.0)*y1f[j])
                    - FullFloat(0.5) * ((y1f[j+1]*y1f[j+1] - y1f[j-1]*y1f[j-1])/(FullFloat(2.0)*dx))
                ) + (dt * butcherB[2]) * (
                    beta/(dx*dx) * (y2f[j+1] + y2f[j-1] - FullFloat(2.0)*y2f[j])
                    - FullFloat(0.5) * ((y2f[j+1]*y2f[j+1] - y2f[j-1]*y2f[j-1])/(FullFloat(2.0)*dx))
                ) + (dt * butcherB[3]) * (
                    beta/(dx*dx) * (y3f[j+1] + y3f[j-1] - FullFloat(2.0)*y3f[j])
                    - FullFloat(0.5) * ((y3f[j+1]*y3f[j+1] - y3f[j-1]*y3f[j-1])/(FullFloat(2.0)*dx))
                ) + (dt * butcherB[4]) * (
                    beta/(dx*dx) * (y4f[j+1] + y4f[j-1] - FullFloat(2.0)*y4f[j])
                    - FullFloat(0.5) * ((y4f[j+1]*y4f[j+1] - y4f[j-1]*y4f[j-1])/(FullFloat(2.0)*dx))
                )
            )
        end
        u = unp1
        t_total += dt
    end

    return u
end

######### Main

#initial conditions
beta = FullFloat(1e-2);
ti = FullFloat(0.0);
tf = FullFloat(1.0);
xi = FullFloat(0.0);
xf = FullFloat(1.0);

nt = parse(Int, ARGS[3]);
nx = parse(Int, ARGS[4]);

ts = collect(range(ti, tf, nt+1)[1:nt]);
xs = collect(range(xi, xf, nx));

#BenchmarkTools.DEFAULT_PARAMETERS.samples = 4;
#@btime novelA_driver(nt, tf, nx, beta, ts, xs)

#=
This takes a long time to run for Float128/BigFloat for large nt.
To make sure everything gets done in a reasonable amount of time,
we run once and if that time is greater than 10min we assume that
no averaging is needed on runtime. At this point we can safely 
ignore system noise and know that it is the method taking up all
the time. Unfortunately in Julia this means the inclusion of compile
time, but I think it is safe to assume that for times greater than
this threshold compile time can be treated as noise.
=#
elapsed_time = @elapsed begin
    u = novelA_driver(nt, tf, nx, beta, ts, xs)
end;

if elapsed_time < 60 * 10 # 10min  
    global tot_elapsed = 0.0
    nruns = 4
    for k=1:nruns
        elapsed_time_ave = @elapsed begin
            uel = novelA_driver(nt, tf, nx, beta, ts, xs)
        end;
        #println("One done in: $elapsed_time")
        global tot_elapsed = tot_elapsed + elapsed_time_ave
    end
    tot_elapsed = tot_elapsed / nruns
    println("Ave elapsed time: $tot_elapsed")
else 
    println("Ave elapsed time: $elapsed_time")
end

#verify the error in quad precision (on x86_64)
u_ref = zeros(Float128, nx)
open("./ref_sol/ref_sol_$nx.txt") do f 
    for (i, line) in enumerate(eachline(f))
        u_ref[i] = parse(Float128, line)
    end
end

dx = Float128(xs[2] - xs[1])
tmp = convert(Array{Float128}, u) - u_ref
l2_err = sqrt.(dx * dot(tmp, tmp))
println("L2 error with reference: $l2_err")
#printstyled("L2 error with reference: ")
#printstyled("$l2_err \n"; bold=true)
