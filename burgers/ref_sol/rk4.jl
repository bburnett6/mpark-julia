using Quadmath;
using DelimitedFiles;
using LinearAlgebra;
using TimerOutputs;

##########################################
# Usage
#
# julia rk4.jl FullType ReduType nt nx write_ref
# Example:
# julia rk4.jl Float128 Float128 1000000 100 1
#
##########################################

# Types for mixed precision using type aliasing
const FullFloat = eval(Meta.parse(ARGS[1]));
const ReduFloat = eval(Meta.parse(ARGS[2]));

function fi(y::Array{FullFloat}, n::Integer, dt::FullFloat, dx::FullFloat, β::FullFloat)::Array{FullFloat}
	fn = zeros(FullFloat, n)

	for i=2:n-1
		#f = β u_xx - 1/2(u^2)_x 
		fn[i] = (
            β/(dx*dx) * (y[i+1] + y[i-1] - FullFloat(2.0) * y[i]) 
            - (y[i+1]*y[i+1] - y[i-1]*y[i-1]) / (FullFloat(4.0)*dx)
            )
	end

	return fn
end

function init_u(xs::Array{FullFloat}, n::Integer)::Array{FullFloat}
    #u = zeros(Float64, n)
    #for i=1:n-1
    #    u[i] = sin(2.0 * pi * xs[i])
    #end
    #Equivalent to
    u = sin.(FullFloat(2.0) * pi * xs)
    u[n] = 0.0 #make sure end boundary is 0.0
    return u
end

function rk4_driver(nt::Integer, tf::FullFloat, nx::Integer, ts::Array{FullFloat}, xs::Array{FullFloat}, beta::FullFloat)::Array{FullFloat}
    dt = ts[2] - ts[1]
    dx = xs[2] - xs[1]
    u = init_u(xs, nx)
    t_total = FullFloat(ts[1])
    
    for (i, t) in enumerate(ts)
        if i == nt
            dt = tf - t_total
        end
        k1 = fi(u, nx, t, dx, beta)
        tmp = u + dt/FullFloat(2.0) * k1 
        k2 = fi(tmp, nx, t + dt/FullFloat(2.0), dx, beta)
        tmp = u + dt/FullFloat(2.0) * k2 
        k3 = fi(tmp, nx, t + dt/FullFloat(2.0), dx, beta)
        tmp = u + dt * k3 
        k4 = fi(tmp, nx, t + dt, dx, beta)

        u = u + FullFloat(1.0)/FullFloat(6.0) * dt * (k1 + FullFloat(2.0)*k2 + FullFloat(2.0)*k3 + k4)
        t_total += dt 
    end
    #println("Total time: $t_total")
    
    return u
end

####### Main

#initial conditions
beta = FullFloat(1e-2);
ti = FullFloat(0.0);
tf = FullFloat(1.0);
xi = FullFloat(0.0);
xf = FullFloat(1.0);

nt = parse(Int, ARGS[3]);
nx = parse(Int, ARGS[4]);
write_ref = parse(Int, ARGS[5]);

ts = collect(range(ti, tf, nt+1)[1:nt]);
xs = collect(range(xi, xf, nx));

elapsed_time = @elapsed begin
    u = rk4_driver(nt, tf, nx, ts, xs, beta)
end;

println("Method time: $elapsed_time")

if write_ref == 1
    open("ref_sol_$nx.txt", "w") do io 
        writedlm(io, u)
    end 
end 

#This is broken. Tryparse fails and is unable to read float128
#u_ref = readdlm("ref_sol_$nx.txt", '\t', FullFloat, '\n')
u_ref = zeros(FullFloat, nx)
open("ref_sol_$nx.txt") do f 
    for (i, line) in enumerate(eachline(f))
        u_ref[i] = parse(FullFloat, line)
    end
end

dx = xs[2] - xs[1]
tmp = u - u_ref
l2_err = sqrt.(dx * dot(tmp, tmp))
println("L2 error with reference: $l2_err")