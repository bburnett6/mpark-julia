
using Quadmath;
#using NLsolve;
#using BenchmarkTools;
using LinearAlgebra;
using TimerOutputs;

##########################################
# Usage
#
# julia -t n_threads sdirk.jl FullType ReduType nt nx 
# Example
# julia -t 4 sdirk.jl Float64 Float32 100000 20
#
##########################################

#BLAS threading
BLAS.set_num_threads(Threads.nthreads())

const FullFloat = eval(Meta.parse(ARGS[1]));
const ReduFloat = eval(Meta.parse(ARGS[2]));

############ Functions

function burgers_sdirk_F!(n::Integer, β::ReduFloat, dx::ReduFloat, dt1::ReduFloat, u::Array{ReduFloat}, F::Array{ReduFloat}, y::Array{ReduFloat})
    F[1] = ReduFloat(0.0)
    F[n] = ReduFloat(0.0)
    for i=2:n-1
        #implicit burgers step F = 0 for y = u + dt * f(y)
        # F = u - y + dt/2 * (βYxx - 1/2(Y^2)x)
        F[i] = (u[i] - y[i] 
            + dt1 * (
                (β/(dx*dx)) * (y[i+1] + y[i-1] - ReduFloat(2.0) * y[i]) 
                - (ReduFloat(0.5) / (ReduFloat(2.0)*dx)) * (y[i+1]*y[i+1] - y[i-1]*y[i-1])
                )
            )    
    end
end

function burgers_sdirk_J!(n::Integer, β::ReduFloat, dx::ReduFloat, dt1::ReduFloat, J::Matrix{ReduFloat}, y::Array{ReduFloat})
    J[:, :] .= ReduFloat(0.0)
    J[1, 1] = ReduFloat(1.0) #Set to 1 for Dirichlet boundary conditions
    J[n, n] = ReduFloat(1.0)
    for i=2:n-1
        #Jacobian for the implicit burgers step F=0 for y = u + dt * f(y)
        # F = u - y + dt * (βYxx - 1/2(Y^2)x)
        J[i, i] = ReduFloat(-1.0) + dt1 * β * ReduFloat(-2.0) / (dx*dx) 
        J[i, i+1] = dt1 * ((β/(dx*dx)) - y[i+1]/(ReduFloat(2.0) * dx))
        J[i, i-1] = dt1 * ((β/(dx*dx)) + y[i-1]/(ReduFloat(2.0) * dx))
    end
end

#=
My newtons method. Takes in inital guess x0 and mutates it to the solution over
max_iter iterations. Fn and Jn are preallocated placeholders for the iteration
computations. 
=#
function my_newtons!(f!, j!, x0::Array{ReduFloat}, Fn::Array{ReduFloat}, Jn::Matrix{ReduFloat})::Array{ReduFloat}
    tol = eps(ReduFloat) * .5e1; #Tolerance of the solver slightly larger than machine epsilon for that precision
    max_iter = 20; #maximum number of iterations for the solver 

    #println("starting newtons")
    for i=1:max_iter
        f!(Fn, x0)
        if norm(Fn) < tol 
            #println("iter: $i, tol: $(norm(Fn))")
            return x0
        end

        j!(Jn, x0)

        #x0 = x0 - inv(Jn) * Fn;
        x0 = x0 - Jn \ Fn;
        #println(norm(Fn))
    end

    return x0
end

function init_u(xs::Array{FullFloat}, n::Integer)::Array{FullFloat}
    #u = zeros(Float64, n)
    #for i=1:n-1
    #    u[i] = sin(2.0 * pi * xs[i])
    #end
    #Equivalent to
    u = sin.(FullFloat(2.0) * pi * xs)
    u[n] = 0.0 #make sure end boundary is 0.0
    return u
end

function sdirk_driver(nt::Integer, tf::FullFloat, nx::Integer, beta::FullFloat, ts::Array{FullFloat}, xs::Array{FullFloat}, ncorr::Integer)::Array{FullFloat}
    dt = ts[2] - ts[1]
    dx = xs[2] - xs[1]
    t_total = FullFloat(ts[1])
    u = init_u(xs, nx)
    unp1 = copy(u)
    fn = zeros(ReduFloat, nx)
    jn = zeros(ReduFloat, nx, nx)
    γ_full = FullFloat((sqrt(3.0) + 3.0) / 6.0)
    γ = ReduFloat((sqrt(3.0) + 3.0) / 6.0)
    dt1 = γ * ReduFloat(dt)
    dt2 = γ * ReduFloat(dt)

    #jacobian function doesn't change each time step so leave it out of the loop
    j!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = burgers_sdirk_J!(nx, ReduFloat(beta), ReduFloat(dx), dt1, J, y)
    
    for (i, t) in enumerate(ts)
        #Make sure final time is reached
        if i == nt
            dt = tf - t_total
        end
        #implicit step 1
        tmp_u = convert(Array{ReduFloat}, u)
        #implicit function changes with u, so redefine each time step
        f!(F::Array{ReduFloat}, y::Array{ReduFloat}) = burgers_sdirk_F!(nx, ReduFloat(beta), ReduFloat(dx), dt1, tmp_u, F, y)
        #y1 = nlsolve(f!, j!, tmp_u).zero #solve the nonlinear system f! using initial guess u
        y1 = my_newtons!(f!, j!, tmp_u, fn, jn)

        #First correction step
        y1k = convert(Array{FullFloat}, y1)
        for k=1:ncorr
            for j=2:nx-1
                y1k[j] = (u[j] + γ_full*dt * (
                    beta/(dx*dx) * (y1k[j+1] + y1k[j-1] - FullFloat(2.0)*y1k[j]) 
                    - FullFloat(0.5) * ((y1k[j+1]*y1k[j+1] - y1k[j-1]*y1k[j-1])/(FullFloat(2.0)*dx))
                    ))
            end
        end
        
        #implicit step 2
        #for corrections, tmp_u2 is done at full precision then converted
        tmp_u2 = zeros(FullFloat, nx)
        for j=2:nx-1
            tmp_u2[j] = (u[j] + (FullFloat(1.0)-FullFloat(2.0)*γ_full)*dt * (
                (beta/(dx*dx)) * (y1k[j+1] + y1k[j-1] - FullFloat(2.0)*y1k[j])
                - FullFloat(0.5) * ((y1k[j+1]*y1k[j+1] - y1k[j-1]*y1k[j-1])/(FullFloat(2.0)*dx))
                ))
        end
        tmp_u2r = ReduFloat.(tmp_u2)
        f2!(F::Array{ReduFloat}, y::Array{ReduFloat}) = burgers_sdirk_F!(nx, ReduFloat(beta), ReduFloat(dx), dt1, tmp_u2r, F, y)
        #y2 = nlsolve(f2!, j!, tmp_u2).zero #solve the nonlinear system f! using initial guess u
        y2 = my_newtons!(f2!, j!, tmp_u2r, fn, jn)
        
        #second correction step
        y2k = convert(Array{FullFloat}, y2)
        for k=1:ncorr
            for j=2:nx-1
                y2k[j] = (u[j] +
                    (FullFloat(1.0)-FullFloat(2.0)*γ_full)*dt * (
                        beta/(dx*dx) * (y1k[j+1] + y1k[j-1] - FullFloat(2.0)*y1k[j]) 
                        - FullFloat(0.5) * ((y1k[j+1]*y1k[j+1] - y1k[j-1]*y1k[j-1])/(FullFloat(2.0)*dx))
                    ) +
                    γ_full*dt * (
                        beta/(dx*dx) * (y2k[j+1] + y2k[j-1] - FullFloat(2.0)*y2k[j]) 
                        - FullFloat(0.5) * ((y2k[j+1]*y2k[j+1] - y2k[j-1]*y2k[j-1])/(FullFloat(2.0)*dx))
                    )
                )
            end
        end

        #explicit step
        unp1[1] = u[1]
        for j=2:nx-1
            unp1[j] = (u[j] +
                FullFloat(0.5)*dt * (
                    beta/(dx*dx) * (y1k[j+1] + y1k[j-1] - FullFloat(2.0)*y1k[j]) 
                    - FullFloat(0.5) * ((y1k[j+1]*y1k[j+1] - y1k[j-1]*y1k[j-1])/(FullFloat(2.0)*dx))
                    ) +
                FullFloat(0.5)*dt * (
                    beta/(dx*dx) * (y2k[j+1] + y2k[j-1] - FullFloat(2.0)*y2k[j]) 
                    - FullFloat(0.5) * ((y2k[j+1]*y2k[j+1] - y2k[j-1]*y2k[j-1])/(FullFloat(2.0)*dx))
                    )
                )
        end
        unp1[nx] = u[nx]
        
        #update
        u = unp1
        t_total += dt 
    end
    
    return u
end

######### Main

#initial conditions
beta = FullFloat(1e-2);
ti = FullFloat(0.0);
tf = FullFloat(1.0);
xi = FullFloat(0.0);
xf = FullFloat(1.0);

nt = parse(Int, ARGS[3]);
nx = parse(Int, ARGS[4]);
ncorr = parse(Int, ARGS[5]);

ts = collect(range(ti, tf, nt+1)[1:nt]);
xs = collect(range(xi, xf, nx));

#BenchmarkTools.DEFAULT_PARAMETERS.samples = 4;
#@btime sdirk_driver(nt, tf, nx, beta, ts, xs)

#=
This takes a long time to run for Float128/BigFloat for large nt.
To make sure everything gets done in a reasonable amount of time,
we run once and if that time is greater than 10min we assume that
no averaging is needed on runtime. At this point we can safely 
ignore system noise and know that it is the method taking up all
the time. Unfortunately in Julia this means the inclusion of compile
time, but I think it is safe to assume that for times greater than
this threshold compile time can be treated as noise.
=#
elapsed_time = @elapsed begin
    u = sdirk_driver(nt, tf, nx, beta, ts, xs, ncorr)
end;

if elapsed_time < 60 * 10 # 10min  
    global tot_elapsed = 0.0
    nruns = 4
    for k=1:nruns
        elapsed_time_ave = @elapsed begin
            uel = sdirk_driver(nt, tf, nx, beta, ts, xs, ncorr)
        end;
        #println("One done in: $elapsed_time")
        global tot_elapsed = tot_elapsed + elapsed_time_ave
    end
    tot_elapsed = tot_elapsed / nruns
    println("Ave elapsed time: $tot_elapsed")
else 
    println("Ave elapsed time: $elapsed_time")
end

#verify the error in quad precision (on x86_64)
u_ref = zeros(Float128, nx)
open("./ref_sol/ref_sol_$nx.txt") do f 
    for (i, line) in enumerate(eachline(f))
        u_ref[i] = parse(Float128, line)
    end
end

dx = Float128(xs[2] - xs[1])
tmp = convert(Array{Float128}, u) - u_ref
l2_err = sqrt.(dx * dot(tmp, tmp))
println("L2 error with reference: $l2_err")

