
using Quadmath;
using NLsolve;
#using BenchmarkTools;
using LinearAlgebra;
using TimerOutputs;
using Plots;

##########################################
# Usage
#
# julia -t n_threads sdirk.jl FullType ReduType nt nx 
# Example
# julia -t 4 sdirk.jl Float64 Float32 100000 20
#
##########################################

#BLAS threading
BLAS.set_num_threads(Threads.nthreads())

const FullFloat = eval(Meta.parse(ARGS[1]));
const ReduFloat = eval(Meta.parse(ARGS[2]));

############ Functions

function burgers_sdirk_F1!(n::Integer, β::ReduFloat, dx::ReduFloat, dt::ReduFloat, u::Array{ReduFloat}, F::Array{ReduFloat}, y::Array{ReduFloat})
    F[1] = ReduFloat(0.0)
    F[n] = ReduFloat(0.0)
    γ = ReduFloat((sqrt(3.0) + 3.0) / 6.0)

    for i=2:n-1
        #first implicit burgers step F = 0 for y = u + γ*dt * f(y)
        # F = u - Y + γ*dt * (βYxx - 1/2(Y^2)x)
        #=
        F[i] = (u[i] - y[i] 
            + dt*γ * (
            β/(dx*dx) * (y[i+1] + y[i-1] - ReduFloat(2.0) * y[i]) 
            - ReduFloat(0.5) * (y[i+1]*y[i+1] - y[i-1]*y[i-1]) / (ReduFloat(2.0)*dx))
            )  
        =#
        #=
        F[i] = (u[i] - y[i]
            + γ*dt * (
                β/(dx*dx) * (y[i+1] - y[i-1] - ReduFloat(2.0)*y[i])
                - ReduFloat(0.25)/dx * (y[i+1]*y[i+1] - y[i-1]*y[i-1])
                )
            )  
        =#
        F[i] = u[i] - y[i]
        F[i] += γ*dt * ( β/(dx*dx) * (y[i+1] - y[i-1] - ReduFloat(2.0)*y[i]) - ReduFloat(0.25)/dx * (y[i+1]*y[i+1] - y[i-1]*y[i-1]) )
    end
end

function burgers_sdirk_F2!(n::Integer, β::ReduFloat, dx::ReduFloat, dt::ReduFloat, y1::Array{ReduFloat}, u::Array{ReduFloat}, F::Array{ReduFloat}, y2::Array{ReduFloat})
    F[1] = ReduFloat(0.0)
    F[n] = ReduFloat(0.0)
    γ = ReduFloat((sqrt(3.0) + 3.0) / 6.0)

    for i=2:n-1
        #second implicit burgers step F = 0 for y = u + (1-2γ)*dt * f(y1) + γ*dt * f(y)
        # F = u - Y2 + (1-2γ)*dt * (βY1xx - 1/2(Y1^2)x) + γ*dt * (βY2xx - 1/2(Y2^2)x)
        #=
        F[i] = (u[i] - y2[i]
            + (ReduFloat(1.0)-ReduFloat(2.0)*γ)*dt * (
                β/(dx*dx) * (y1[i+1] + y1[i-1] - ReduFloat(2.0)*y1[i])
                - ReduFloat(0.25)/dx * (y1[i+1]*y1[i+1] - y1[i-1]*y1[i-1])
                )
            + γ*dt * (
                β/(dx*dx) * (y2[i+1] + y2[i-1] - ReduFloat(2.0)*y2[i])
                - ReduFloat(0.25)/dx * (y2[i+1]*y2[i+1] - y2[i-1]*y2[i-1])
                )
            )   
        =#
        F[i] = u[i] - y2[i]
        F[i] += (ReduFloat(1.0)-ReduFloat(2.0)*γ)*dt * ( β/(dx*dx) * (y1[i+1] + y1[i-1] - ReduFloat(2.0)*y1[i]) - ReduFloat(0.25)/dx * (y1[i+1]*y1[i+1] - y1[i-1]*y1[i-1]) )
        F[i] += γ*dt * ( β/(dx*dx) * (y2[i+1] + y2[i-1] - ReduFloat(2.0)*y2[i]) - ReduFloat(0.25)/dx * (y2[i+1]*y2[i+1] - y2[i-1]*y2[i-1]) )
    end
end

function burgers_sdirk_J!(n::Integer, β::ReduFloat, dx::ReduFloat, dt::ReduFloat, J::Matrix{ReduFloat}, y::Array{ReduFloat})
    J .= ReduFloat(0.0)
    γ = ReduFloat((sqrt(3.0) + 3.0) / 6.0)

    for i=1:n
        #Jacobian for the implicit burgers step F=0 for y = u + γ*dt * f(y)
        #this is the same Jacobian for the second stage too F = 0 for y2 = u + (1-2γ)*dt * f(y1) + γ*dt * f(y2)
        #Term with y1 gets removed by derivatives so really its just y2 = u + γ*dt * f(y2)
        # F = u - y + γ*dt * (βYxx - 1/2(Y^2)x)
        J[i, i] = ReduFloat(-1.0) + dt*γ * β * ReduFloat(-2.0) / (dx*dx) 
        if i-1 != 0
            J[i, i-1] = dt*γ * (β / (dx*dx) + ReduFloat(0.5)/dx * y[i-1])
        end
        if i+1 != n+1 
            J[i, i+1] = dt*γ * (β / (dx*dx) - ReduFloat(0.5)/dx * y[i+1])
        end
    end
end

#=
My newtons method. Takes in inital guess x0 and mutates it to the solution over
max_iter iterations. Fn and Jn are preallocated placeholders for the iteration
computations. 
=#
function my_newtons!(f!, j!, x0::Array{ReduFloat}, Fn::Array{ReduFloat}, Jn::Matrix{ReduFloat})::Array{ReduFloat}
    tol = eps(ReduFloat) * .5e1; #Tolerance of the solver slightly larger than machine epsilon for that precision
    max_iter = 20; #maximum number of iterations for the solver 

    #println("starting newtons")
    for i=1:max_iter
        f!(Fn, x0)
        if norm(Fn) < tol 
            #println("iter: $i, tol: $(norm(Fn))")
            return x0
        end

        j!(Jn, x0)

        #x0 = x0 - inv(Jn) * Fn;
       	x0 = x0 - Jn \ Fn;
        #println(norm(Fn))
    end
    return x0
end

function init_u(xs::Array{FullFloat}, n::Integer)::Array{FullFloat}
    #u = zeros(Float64, n)
    #for i=1:n-1
    #    u[i] = sin(2.0 * pi * xs[i])
    #end
    #Equivalent to
    u = sin.(FullFloat(2.0) * pi * xs)
    u[n] = 0.0 #make sure end boundary is 0.0
    return u
end

function sdirk_driver(nt::Integer, nx::Integer, beta::FullFloat, ts::Array{FullFloat}, xs::Array{FullFloat})::Array{FullFloat}
    dt = ts[2] - ts[1]
    dx = xs[2] - xs[1]
    u = init_u(xs, nx)
    unp1 = copy(u)
    fn = zeros(ReduFloat, nx)
    jn = zeros(ReduFloat, nx, nx)
    γ = ReduFloat((sqrt(3.0) + 3.0) / 6.0)
    #jacobian doesn't change each time step and is the same function just
    #with different parameter y, so leave it out of the loop
    j!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = burgers_sdirk_J!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt), J, y)

    nframes = 50
    frame_every = nt ÷ nframes
    anim = Animation()
    plt = plot(xs, u, lim=(-1.0, 1.0), xlim=(xs[1], xs[nx]), title="t = 0")
    
    for (i, t) in enumerate(ts)
        #first implicit step
        tmp_u = convert(Array{ReduFloat}, u)
        #implicit function changes with u, so redefine each time step
        f!(F::Array{ReduFloat}, y::Array{ReduFloat}) = burgers_sdirk_F1!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt), tmp_u, F, y)
        #y1 = my_newtons!(f!, j!, tmp_u, fn, jn)
        y1 = nlsolve(f!, tmp_u).zero

        #second implicit step
        #tmp_u2 = convert(Array{ReduFloat}, u)
        #for j=2:nx-1
        #    tmp_u2[j] += (ReduFloat(1.0)-ReduFloat(2.0)*γ)*dt * (beta/(dx*dx) * (y1[j+1] + y1[j-1] - ReduFloat(2.0)*y1[j]) - ReduFloat(0.25)/dx * (y1[j+1]*y1[j+1] - y1[j-1]*y1[j-1]))
        #end
        #f2!(F::Array{ReduFloat}, y::Array{ReduFloat}) = burgers_sdirk_F1!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt), tmp_u2, F, y)
        f2!(F::Array{ReduFloat}, y::Array{ReduFloat}) = burgers_sdirk_F2!(nx, ReduFloat(beta), ReduFloat(dx), ReduFloat(dt), y1, tmp_u, F, y)
        #y2 = my_newtons!(f2!, j!, tmp_u, fn, jn)
        y2 = nlsolve(f2!, tmp_u).zero
        
        #explicit step
        y1f = convert(Array{FullFloat}, y1)
        y2f = convert(Array{FullFloat}, y2)
        unp1[1] = u[1]
        for j=2:nx-1
            #=
            unp1[j] = (u[j]
                + FullFloat(0.5)*dt * (
                    beta/(dx*dx) * (y1f[j+1] + y1f[j-1] - FullFloat(2.0)*y1f[j])
                    - FullFloat(0.25)/dx * (y1f[j+1]*y1f[j+1] - y1f[j-1]*y1f[j-1])
                    )
                + FullFloat(0.5)*dt * (
                    beta/(dx*dx) * (y2f[j+1] + y2f[j-1] - FullFloat(2.0)*y2f[j])
                    - FullFloat(0.25)/dx * (y2f[j+1]*y2f[j+1] - y2f[j-1]*y2f[j-1])
                    )
                )
            =#
            unp1[j] = u[j]
            unp1[j] += FullFloat(0.5)*dt * (beta/(dx*dx) * (y1f[j+1] + y1f[j-1] - FullFloat(2.0)*y1f[j]) - FullFloat(0.25)/dx * (y1f[j+1]*y1f[j+1] - y1f[j-1]*y1f[j-1]))
            unp1[j] += FullFloat(0.5)*dt * (beta/(dx*dx) * (y2f[j+1] + y2f[j-1] - FullFloat(2.0)*y2f[j])- FullFloat(0.25)/dx * (y2f[j+1]*y2f[j+1] - y2f[j-1]*y2f[j-1]))
        end
        unp1[nx] = u[nx]
        
        #update
        u = unp1

        if mod(i, frame_every) == 0
            plt = plot(xs, u, lim=(-1.0, 1.0), xlim=(xs[1], xs[nx]), title="t = $t")
            frame(anim, plt)
        end
    end
    
    gif(anim, "./burgers_mysdirk_nt$(nt)_nx$(nx).gif", fps=10)
    return u
end

######### Main

#initial conditions
beta = FullFloat(1e-2);
ti = FullFloat(0.0);
tf = FullFloat(1.0);
xi = FullFloat(0.0);
xf = FullFloat(1.0);

nt = parse(Int, ARGS[3]);
nx = parse(Int, ARGS[4]);

ts = collect(range(ti, tf, nt+1)[1:nt]);
xs = collect(range(xi, xf, nx));

elapsed_time = @elapsed begin
    u = sdirk_driver(nt, nx, beta, ts, xs)
end;
println("Method Time: $elapsed_time")

u_ref = zeros(Float128, nx)
open("./ref_sol/ref_sol_$nx.txt") do f 
    for (i, line) in enumerate(eachline(f))
        u_ref[i] = parse(Float128, line)
    end
end

dx = Float128(xs[2] - xs[1])
tmp = convert(Array{Float128}, u) - u_ref
l2_err = sqrt.(dx * dot(tmp, tmp))
println("L2 error with reference: $l2_err")
