
using Quadmath;
#using BenchmarkTools;
using LinearAlgebra;
using TimerOutputs;

##########################################
# Usage
#
# julia -t n_threads sdirk.jl FullType ReduType nt lambda
# Example
# julia -t 4 sdirk.jl Float64 Float32 100000
#
##########################################

#BLAS threading
BLAS.set_num_threads(Threads.nthreads())

const FullFloat = eval(Meta.parse(ARGS[1]));
const ReduFloat = eval(Meta.parse(ARGS[2]));

############ Functions

function prothero_sdirk_F(y::ReduFloat, u::ReduFloat, t::ReduFloat, dt::ReduFloat, λ::ReduFloat)::ReduFloat
    fn = u + dt * (λ * (y - sin(t)) + cos(t)) - y
    #println("Fn in: $fn")
    return fn 
end

function prothero_sdirk_Fp(y::ReduFloat, dt::ReduFloat, λ::ReduFloat)::ReduFloat
    fp = dt * λ - ReduFloat(1.0)
    return fp
end

function newtons(x0::ReduFloat, u::ReduFloat, t::ReduFloat, dt::ReduFloat, λ::ReduFloat)::ReduFloat
    tol = eps(ReduFloat) * .5e1; #Tolerance of the solver slightly larger than machine epsilon for that precision
    max_iter = 20; #maximum number of iterations for the solver 

    #println("starting newtons")
    for i=1:max_iter
        Fn = prothero_sdirk_F(x0, u, t, dt, λ)
        if abs(Fn) < tol 
            #println("iter: $i, Fn: $(abs(Fn))")
            return x0
        end

        Fnp = prothero_sdirk_Fp(x0, dt, λ)

        x0 = x0 - Fn / Fnp;
    end
    return x0
end

function implicit_step(u::ReduFloat, t::ReduFloat, dt::ReduFloat, λ::ReduFloat)::ReduFloat
    y = (u + dt*(cos(t) - λ*sin(t))) / (ReduFloat(1.0) - λ*dt)
    return y 
end

function init_u(ti)::FullFloat
    u = sin(ti)
    return u
end

function sdirk_driver(nt::Integer, tf::FullFloat, λ::FullFloat, ts::Array{FullFloat})::FullFloat
    dt = ts[2] - ts[1]
    t_total = FullFloat(ts[1])
    u = init_u(ts[1])
    gamma = FullFloat((sqrt(3.0) + 3.0) / 6.0);
    γ = ReduFloat(gamma);
    
    for (i, t) in enumerate(ts)
        #Make sure final time is reached correctly
        if i == nt 
            dt = tf - t_total
        end 

        #implicit step 1
        t1 = t + gamma*dt 
        dt1 = gamma*dt
        tmp_u = ReduFloat(u)
        #y1 = newtons(ReduFloat(u), tmp_u, ReduFloat(t1), ReduFloat(dt1), ReduFloat(λ))
        y1 = implicit_step(tmp_u, ReduFloat(t1), ReduFloat(dt1), ReduFloat(λ))
        y1f = FullFloat(y1)

        #implicit step 2 
        t2 = t + (FullFloat(1.0) - gamma) * dt 
        dt1 = dt * (FullFloat(1.0) - FullFloat(2.0)*gamma)
        dt2 = dt * gamma
        tmp_u = ReduFloat(u + dt1 * (λ * (y1f - sin(t1)) + cos(t1)))
        #y2 = newtons(ReduFloat(u), tmp_u, ReduFloat(t2), ReduFloat(dt2), ReduFloat(λ))
        y2 = implicit_step(tmp_u, ReduFloat(t2), ReduFloat(dt2), ReduFloat(λ))
        y2f = FullFloat(y2)

        #explicit step
        u = u + FullFloat(0.5)*dt * (λ * (y1f - sin(t1)) + cos(t1)) + FullFloat(0.5)*dt * (λ * (y2f - sin(t2)) + cos(t2))
        t_total += dt
    end
    
    return u
end

######### Main

#initial conditions
ti = FullFloat(0.0);
tf = FullFloat(0.5);

nt = parse(Int, ARGS[3]);
lambda = parse(FullFloat, ARGS[4])

ts = collect(range(ti, tf, nt+1)[1:nt]);

u = sdirk_driver(nt, tf, lambda, ts)

global tot_elapsed = 0.0
nruns = 4
for k=1:nruns
    elapsed_time = @elapsed begin
        uel = sdirk_driver(nt, tf, lambda, ts)
    end;
    #println("One done in: $elapsed_time")
    global tot_elapsed = tot_elapsed + elapsed_time 
end
tot_elapsed = tot_elapsed / nruns
println("Ave elapsed time: $tot_elapsed")

u_exact = sin(Float128(tf))

tmp = Float128(u) - u_exact
l2_err = sqrt.(dot(tmp, tmp))
println("L2 error with reference: $l2_err")
