
import jinja2 #Tutorial used: https://zetcode.com/python/jinja/
import subprocess
import itertools
import argparse as ap 
import time
import sys
import os

class SbatchOption:
	"""
	Simple class that represents an sbatch option and its value.
	Option names should be their full name not the abreviated name.
	See `man sbatch` for a list of all options.

	ex:
	SbatchOption("nodes", "1")
	SbatchOption("ntasks-per-node", "8")
	"""
	def __init__(self, name, value):
		self.name = name 
		self.value = value 

class HeateqCmdArgs:
	"""
	Arguments to use the impmid.jl and impmid-corrected.jl files.
	"""
	def __init__(self, n_threads, filename, fulltype, redutype, nt, ncorr):
		self.n_threads = n_threads
		self.filename = filename
		self.fulltype = fulltype
		self.redutype = redutype
		self.nt = nt 
		self.ncorr = ncorr
		self.is_corr = True
		if int(ncorr) == 0:
			self.is_corr = False
		#print(f"####### {self.ncorr} {self.is_corr}")


def run_for(method_name, n_threads, ts, fulltype, redutype, ncorr):
	output_basedir = f"output_threads-{n_threads}/{method_name}/corr-{ncorr}"
	error_basedir = f"error_threads-{n_threads}/{method_name}/corr-{ncorr}"

	throttle = "%1" #only run one from each array at a time
	options = []
	options.append(SbatchOption("job-name", "MPARK-Prothero"))
	options.append(SbatchOption("array", ts + throttle)) 
	options.append(SbatchOption("error", f"{error_basedir}/err_{fulltype}_{redutype}_nt-%a.txt"))
	options.append(SbatchOption("output", f"{output_basedir}/out_{fulltype}_{redutype}_nt-%a.txt"))
	options.append(SbatchOption("partition", "long-single"))
	options.append(SbatchOption("nodes", "1"))
	options.append(SbatchOption("ntasks-per-node", f"{n_threads}"))

	if int(ncorr) == 0:
		fname = f"{method_name}/{method_name}.jl"
	else:
		fname = f"{method_name}/{method_name}-corrections.jl"
	cmd = HeateqCmdArgs(n_threads, fname, fulltype, redutype, '$SLURM_ARRAY_TASK_ID', ncorr)

	#Load and populate template
	file_loader = jinja2.FileSystemLoader('batch-templates')
	env = jinja2.Environment(loader=file_loader)
	#options to alter how the template is rendered
	#env.trim_blocks = True
	#env.lstrip_blocks = True
	#env.rstrip_blocks = True

	template = env.get_template('carnie.sh')
	sbatch_script = template.render(options=options, cmd=cmd)

	#Prepare run for submission by creating error/output dirs
	if not os.path.exists(f'{error_basedir}'):
		os.makedirs(f'{error_basedir}')
	if not os.path.exists(f'{output_basedir}'):
		os.makedirs(f'{output_basedir}')

	subprocess.run(['sbatch'], input=sbatch_script, encoding='ascii')
	#print(sbatch_script)


if __name__ == '__main__':
	parser = ap.ArgumentParser(description='Run a set of batch scripts for the Van der Pol equation in julia')
	parser.add_argument(
		'--ncorr', 
		dest='ncorr', 
		default="0",
		help='Number of corrections'
		)
	parser.add_argument(
		'-t', 
		dest='threads', 
		default="8",
		help='Number of threads used by BLAS'
		)
	parser.add_argument(
		'-m', 
		dest='method', 
		default="8",
		help='Name of method to use'
		)

	args = parser.parse_args()

	n_corr = args.ncorr
	n_threads = args.threads
	method_name = args.method
	
	#Number of timesteps to run for
	#timesteps are indexed using these values from an array in the template batch script
	ts = '1-15'

	#Datatypes to run with
	d_types = ['Float128', 'Float64', 'Float32', 'Float16']
	#d_types = ['Float64', 'Float32']
	type_combos = itertools.combinations_with_replacement(d_types, 2)

	for d_full, d_redu in type_combos:
		run_for(method_name, n_threads, ts, d_full, d_redu, n_corr)
		time.sleep(1) #no spamming queue
	
	#run_for(n_threads, ts, 'Float64', 'Float64', 0)