
import itertools
import numpy as np 
import pandas as pd 
import argparse as ap 
import os 

def gen_table(method_name, ncorr, nthreads, alpha, is_save):
	output_basedir = f"output_threads-{nthreads}/{method_name}/corr-{ncorr}"
	nocorr_basedir = f"output_threads-{nthreads}/{method_name}/corr-0"
	if not os.path.exists(f'{output_basedir}'):
		print('Outputs dont exist. Check args?')
		return

	#Get only the types that are available in the filenames.
	#getFloatType is used for sorting on the integer value of the float type (ie Float128 > Float64)
	getFloatType = lambda s: int(s.split('Float')[-1])
	#types is the set of all available types
	types = list(set(map(lambda s: s.split('_')[1], os.listdir(output_basedir))))
	#sort the types in reverse order based on the getFloatType function
	types.sort(reverse=True, key=getFloatType)
	#get all type combinations
	type_combos = list(itertools.combinations_with_replacement(types, 2))
	#similar process with getting the number of timesteps used
	nts = list(set(map(lambda s: int(s.split('.')[0].split('-')[-1]), os.listdir(output_basedir))))
	nts.sort()

	alphas = ['0.01', '0.1', '1.0', '10.0', '100.0', '1000.0', '10000.0', '100000.0'] #match this with values in batch_templates/carnie.sh
	a_line_index = 2*alphas.index(alpha)

	err_dt = {}
	runt_dt = {}
	ord_dt = {}
	for d_full, d_redu in type_combos:
		name = f'{d_full}/{d_redu}'
		dts = []
		runtimes = []
		errors = []
		orders = []
		for nt in nts:
			dts.append(f'{1/2**nt}')
			filename = f'out_{d_full}_{d_redu}_nt-{nt}.txt'	
			with open(os.path.join(output_basedir, filename), 'r') as f:
				#print(filename)
				output = f.read()
				#print(output)
				lines = output.splitlines()
				runtimes.append(float(lines[a_line_index].split()[-1]))
				errors.append(float(lines[a_line_index+1].split()[-1]))
		#Calculate orders
		orders.append(0.0)
		for i in range(1, len(errors)):
			#orders.append(np.log(errors[i+1]/errors[i]) / np.log(errors[i]/errors[i-1]))
			orders.append(np.log2(errors[i-1]/errors[i]))

		err_dt[name] = pd.Series(errors, index=dts)
		runt_dt[name] = pd.Series(runtimes, index=dts)
		ord_dt[name] = pd.Series(orders, index=dts)

	save_path = f'./tables/{method_name}/alpha{alpha}/corrections{ncorr}'
	if is_save:
		if not os.path.exists(save_path):
			os.makedirs(save_path)
		#https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_markdown.html
		with open(f'{save_path}/error_vs_dt.csv', 'w') as f:
			f.write(pd.DataFrame(err_dt).to_csv(index_label='dt'))
		with open(f'{save_path}/order_vs_dt.csv', 'w') as f:
			f.write(pd.DataFrame(ord_dt).to_csv(index_label='dt'))
		#with open(f'./tables/{method_name}/corrections{ncorr}/runtime_vs_dt.csv', 'w') as f:
		#	f.write(pd.DataFrame(runt_dt).to_csv(index_label='dt'))
	else:
		print("errors")
		print(pd.DataFrame(err_dt))
		print()
		print("orders")
		print(pd.DataFrame(ord_dt))
		#print(pd.DataFrame(runt_dt))


if __name__ == '__main__':
	parser = ap.ArgumentParser(description='Generate a table of the errors for the Prothero-Robinson problem')
	parser.add_argument(
		'--ncorr',
		dest='ncorr',
		default='0',
		help='Number of corrections'
		)
	parser.add_argument(
		'-t', 
		dest='threads', 
		default="8",
		help='Number of threads used by BLAS'
		)
	parser.add_argument(
		'-m', 
		dest='method', 
		default="impmid",
		help='Name of the method used to compute solution'
		)
	parser.add_argument(
		'-a',
		dest='alpha',
		default="1.0",
		help="The stiffness coefficient alpha used. range: [0.001..1000.0] by powers of 10 (and 10**8)"
		)
	parser.add_argument(
		'-s',
		dest='is_save',
		action='store_true',
		help='Save files'
		)
	args = parser.parse_args()

	gen_table(args.method, args.ncorr, args.threads, args.alpha, args.is_save)