
import itertools
import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt 
import argparse as ap 
import os 

def gen_table(method_name, ncorr1, ncorr2, alpha, is_save):

	save_path1 = f'./tables/{method_name}/alpha{alpha}/corrections{ncorr1}'
	save_path2 = f'./tables/{method_name}/alpha{alpha}/corrections{ncorr2}'
	data1 = pd.read_csv(f'{save_path1}/error_vs_dt.csv')
	data2 = pd.read_csv(f'{save_path2}/error_vs_dt.csv')
	dts = data1['dt']

	plt.loglog(data1['dt'], data2['Float64/Float64'], '-rs', linewidth=2, markersize=7, label=f'{ncorr2} Corrections')
	plt.loglog(data1['dt'], data1['Float64/Float64'], ':bo', label=f'{ncorr1} Corrections')
	plt.xlabel('dt')
	plt.ylabel('error')
	plt.ylim(top=1e-1)
	plt.yticks([10**(-x) for x in range(1,17)]) #sdirk
	#plt.xticks(data1['dts'])
	plt.xticks([10**(-x) for x in range(1,6)])
	plt.grid()
	plt.legend(prop={'size': 8}, loc="lower right")
	plt.title(f'{method_name.upper()} Corrections={ncorr1},{ncorr2} Alpha={alpha}')

	plt.show()


if __name__ == '__main__':
	parser = ap.ArgumentParser(description='Generate a table of the errors for the Prothero-Robinson problem')
	parser.add_argument(
		'-n1',
		dest='ncorr1',
		default='0',
		help='Number of corrections'
		)
	parser.add_argument(
		'-n2',
		dest='ncorr2',
		default='0',
		help='Number of corrections'
		)
	parser.add_argument(
		'-m', 
		dest='method', 
		default="impmid",
		help='Name of the method used to compute solution'
		)
	parser.add_argument(
		'-a',
		dest='alpha',
		default="1.0",
		help="The stiffness coefficient alpha used. range: [0.001..1000.0] by powers of 10 (and 10**8)"
		)
	parser.add_argument(
		'-s',
		dest='is_save',
		action='store_true',
		help='Save files'
		)
	args = parser.parse_args()

	gen_table(args.method, args.ncorr1, args.ncorr2, args.alpha, args.is_save)