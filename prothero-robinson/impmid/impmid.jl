
using Quadmath;
#using BenchmarkTools;
using LinearAlgebra;
using TimerOutputs;

##########################################
# Usage
#
# julia -t n_threads impmid.jl FullType ReduType nt lambda
# Example
# julia -t 4 impmid.jl Float64 Float32 100000
#
##########################################

#BLAS threading
BLAS.set_num_threads(Threads.nthreads())

const FullFloat = eval(Meta.parse(ARGS[1]));
const ReduFloat = eval(Meta.parse(ARGS[2]));

############ Functions

function prothero_imr_F(y::ReduFloat, u::ReduFloat, t::ReduFloat, dt::ReduFloat, λ::ReduFloat)::ReduFloat
    fn = u + dt * (λ * (y - sin(t)) + cos(t)) - y
    #println("Fn in: $fn")
    return fn 
end

function prothero_imr_Fp(y::ReduFloat, dt::ReduFloat, λ::ReduFloat)::ReduFloat
    fp = dt * λ - ReduFloat(1.0)
    return fp
end

function newtons(x0::ReduFloat, u::ReduFloat, t::ReduFloat, dt::ReduFloat, λ::ReduFloat)::ReduFloat
    tol = eps(ReduFloat) * .5e1; #Tolerance of the solver slightly larger than machine epsilon for that precision
    max_iter = 20; #maximum number of iterations for the solver 

    #println("starting newtons")
    for i=1:max_iter
        Fn = prothero_imr_F(x0, u, t, dt, λ)
        if abs(Fn) < tol 
            #println("iter: $i, Fn: $(abs(Fn))")
            return x0
        end

        Fnp = prothero_imr_Fp(x0, dt, λ)

        x0 = x0 - Fn / Fnp;
    end
    return x0
end

function implicit_step(u::ReduFloat, t::ReduFloat, dt::ReduFloat, λ::ReduFloat)::ReduFloat
    y = (u + dt*(cos(t) - λ*sin(t))) / (ReduFloat(1.0) - λ*dt)
    return y 
end

function init_u(ti)::FullFloat
    u = sin(ti)
    return u
end

function impmid_driver(nt::Integer, tf::FullFloat, λ::FullFloat, ts::Array{FullFloat})::FullFloat
    dt = ts[2] - ts[1]
    t_total = FullFloat(ts[1])
    u = init_u(ts[1])
    
    for (i, t) in enumerate(ts)
        #Make sure final time is reached correctly
        if i == nt 
            dt = tf - t_total
        end
        t_total += dt 

        #implicit step
        tmp_u = ReduFloat(u)
        #y1 = newtons(tmp_u, ReduFloat(u), ReduFloat(t+dt*0.5), ReduFloat(0.5*dt), ReduFloat(λ))
        y1 = implicit_step(tmp_u, ReduFloat(t+dt*0.5), ReduFloat(0.5*dt), ReduFloat(λ))
        y1f = FullFloat(y1)
        
        #explicit step
        u = u + dt * (λ * (y1f - sin(t+dt*0.5)) + cos(t+dt*0.5))

    end
    
    return u
end

######### Main

#initial conditions
ti = FullFloat(0.0);
tf = FullFloat(0.5);

nt = parse(Int, ARGS[3]);
lambda = parse(FullFloat, ARGS[4])

ts = collect(range(ti, tf, nt+1)[1:nt]);

u = impmid_driver(nt, tf, lambda, ts)

global tot_elapsed = 0.0
nruns = 4
for k=1:nruns
    elapsed_time = @elapsed begin
        uel = impmid_driver(nt, tf, lambda, ts)
    end;
    #println("One done in: $elapsed_time")
    global tot_elapsed = tot_elapsed + elapsed_time 
end
tot_elapsed = tot_elapsed / nruns
println("Ave elapsed time: $tot_elapsed")

u_exact = sin(Float128(tf))

tmp = Float128(u) - u_exact
l2_err = sqrt.(dot(tmp, tmp))
println("L2 error with reference: $l2_err")
