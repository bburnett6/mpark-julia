#!/bin/bash

METHOD=impmid

set -x

for ALPHA in 0.01 0.1 1.0 10.0 100.0
do
	for NCORR in 0 1 2
	do
		python plot_results.py -t 1 --ncorr $NCORR -m $METHOD -a $ALPHA -s
	done
done

# make a gif slicing alpha
# convert -delay 100 -loop 0 images/impmid/corrections2/alpha{0.01,0.1,1.0,10.0,100.0}/err_v_dt.png imr_alpha2.gif