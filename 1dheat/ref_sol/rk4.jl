using Quadmath;
using LinearAlgebra;
using DelimitedFiles;
using TimerOutputs;

##########################################
# Usage
#
# julia rk4.jl FullType ReduType nt nx write_ref
# Example:
# julia rk4.jl Float128 Float128 1000000 100 1
#
##########################################

# Types for mixed precision using type aliasing
const fullFloat = eval(Meta.parse(ARGS[1]));
const reduFloat = eval(Meta.parse(ARGS[2]));

# Initialization functions

function init_a(n::Integer, gamma::fullFloat)::Matrix{fullFloat}
    a = zeros(fullFloat, n, n)
    for i=2:n-1
        a[i, i+1] = gamma
        a[i, i] = fullFloat(-2.0) * gamma
        a[i, i-1] = gamma
    end
    a[1, 1] = fullFloat(1.0)
    a[n, n] = fullFloat(1.0)
    return a
end

function init_b(a::Matrix{fullFloat}, n::Integer, gamma::fullFloat)::Matrix{reduFloat}
    b = Matrix{reduFloat}(I, n, n)
    b = b - convert(Matrix{reduFloat}, a) / reduFloat(2.0)
    return b 
end

function init_u(xs::Array{fullFloat}, n::Integer)::Array{fullFloat}
    #u = zeros(fullFloat, n)
    #for i=1:n-1
    #    u[i] = sin(fullFloat(2.0) * fullFloat(pi) * xs[i])
    #end
    u = sin.(fullFloat(2.0) * fullFloat(pi) * xs)
    u[n] = fullFloat(0.0)
    return u
end

function fi(u::Array{fullFloat}, n::Integer, t::fullFloat, dx::fullFloat, beta::fullFloat)::Array{fullFloat}
    fn = zeros(fullFloat, n)

    for i=2:n-1 
        fn[i] = beta/(dx*dx) * (u[i-1] + u[i+1] - fullFloat(2.0) * u[i])
    end 

    return fn 
end

function rk4_driver(nt::Integer, nx::Integer, ts::Array{fullFloat}, xs::Array{fullFloat}, beta::fullFloat)::Array{fullFloat}
    dt = ts[2] - ts[1]
    dx = xs[2] - xs[1]
    gamma = beta * dt / (dx * dx)
    u = init_u(xs, nx)
    unp1 = u
    a = init_a(nx, gamma)
    b = init_b(a, nx, gamma)
    
    for (i, t) in enumerate(ts)
        k1 = fi(u, nx, t, dx, beta)
        tmp = u + dt/fullFloat(2.0) * k1 
        k2 = fi(tmp, nx, t + dt/fullFloat(2.0), dx, beta)
        tmp = u + dt/fullFloat(2.0) * k2 
        k3 = fi(tmp, nx, t + dt/fullFloat(2.0), dx, beta)
        tmp = u + dt * k3 
        k4 = fi(tmp, nx, t + dt, dx, beta)

        u = u + fullFloat(1.0)/fullFloat(6.0) * dt * (k1 + fullFloat(2.0)*k2 + fullFloat(2.0)*k3 + k4)
    end
    
    return u
end

####### Main

beta = fullFloat(1.0);
ti = fullFloat(0.0);
tf = fullFloat(0.1);
xi = fullFloat(0.0);
xf = fullFloat(1.0);

nt = parse(Int, ARGS[3]);
nx = parse(Int, ARGS[4]);
write_ref = parse(Int, ARGS[5]);

ts = collect(range(ti, tf, nt+1)[1:nt]); #Do not include end point! Use slicing achieve this because: https://github.com/JuliaLang/julia/issues/27097
xs = collect(range(xi, xf, nx)); # do include endpoint

elapsed_time = @elapsed begin
    u = rk4_driver(nt, nx, ts, xs, beta)
end;

println("Method time: $elapsed_time")

if write_ref == 1
    open("ref_sol_$nx.txt", "w") do io 
        writedlm(io, u)
    end 
end 

#This is broken. Tryparse fails and is unable to read float128
#u_ref = readdlm("ref_sol_$nx.txt", '\t', fullFloat, '\n')
u_ref = zeros(fullFloat, nx)
open("ref_sol_$nx.txt") do f 
    for (i, line) in enumerate(eachline(f))
        u_ref[i] = parse(fullFloat, line)
    end
end

dx = xs[2] - xs[1]
tmp = u - u_ref
l2_err = sqrt.(dx * dot(tmp, tmp))
println("L2 error with reference: $l2_err")