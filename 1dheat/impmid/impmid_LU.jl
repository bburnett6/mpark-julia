using Quadmath;
using LinearAlgebra;
using TimerOutputs;

##########################################
# Usage
#
# julia impmid.jl FullType ReduType nt nx 
# Example
# julia impmid.jl Float64 Float32 100000 20
#
##########################################

# Types for mixed precision using type aliasing
fullFloat = eval(Meta.parse(ARGS[1]));
reduFloat = eval(Meta.parse(ARGS[2]));

# Initialization functions

function init_a(n::Integer, gamma::fullFloat)::Matrix{fullFloat}
    a = zeros(fullFloat, n, n)
    for i=2:n-1
        a[i, i+1] = gamma
        a[i, i] = fullFloat(-2.0) * gamma
        a[i, i-1] = gamma
    end
    a[1, 1] = fullFloat(1.0)
    a[n, n] = fullFloat(1.0)
    return a
end

function init_b(a::Matrix{fullFloat}, n::Integer, gamma::fullFloat)::Matrix{reduFloat}
    b = Matrix{reduFloat}(I, n, n)
    b = b - convert(Matrix{reduFloat}, a) / reduFloat(2.0)
    return b 
end

function init_u(xs::Array{fullFloat}, n::Integer)::Array{fullFloat}
    #u = zeros(fullFloat, n)
    #for i=1:n-1
    #    u[i] = sin(fullFloat(2.0) * fullFloat(pi) * xs[i])
    #end
    u = sin.(fullFloat(2.0) * fullFloat(pi) * xs)
    u[n] = fullFloat(0.0)
    return u
end

function impmid_driver(nt::Integer, nx::Integer, ts::Array{fullFloat}, xs::Array{fullFloat}, beta::fullFloat)::Array{fullFloat}
    dt = ts[2] - ts[1]
    dx = xs[2] - xs[1]
    gamma = beta * dt / (dx * dx)
    u = init_u(xs, nx)
    unp1 = u
    a = init_a(nx, gamma)
    b = init_b(a, nx, gamma)
    luB = lu(b) #Returns an LU decomposed object
    
    for (i, t) in enumerate(ts)
        #implicit step
        y1 = luB\convert(Array{reduFloat}, u) #LinearAlgebra will use FB-Subst on an LU object
        #y1 = b\convert(Array{reduFloat}, u) #Slow
        #explicit step
        y1f = convert(Array{fullFloat}, y1)
        unp1[1] = u[1]
        for j=2:nx-1
            unp1[j] = u[j] + dt*beta/(dx*dx) * (y1f[j-1] + y1f[j+1] - fullFloat(2.0) * y1f[j])
        end
        unp1[nx] = u[nx]
        u = unp1
    end
    
    return u
end

####### Main

beta = fullFloat(1.0);
ti = fullFloat(0.0);
tf = fullFloat(0.1);
xi = fullFloat(0.0);
xf = fullFloat(1.0);

nt = parse(Int, ARGS[3]);
nx = parse(Int, ARGS[4]);

ts = collect(range(ti, tf, nt+1)[1:nt]); #Do not include end point! Use slicing achieve this because: https://github.com/JuliaLang/julia/issues/27097
xs = collect(range(xi, xf, nx)); # do include endpoint

#global tot_elapsed = 0.0
#nruns = 5
#for k=1:nruns
#    elapsed_time = @elapsed begin
#        u = impmid_driver(nt, nx, ts, xs, beta)
#    end;
#    global tot_elapsed = tot_elapsed + elapsed_time 
#end
#tot_elapsed = tot_elapsed / nruns
#println("Ave elapsed time: $tot_elapsed")

elapsed_time = @elapsed begin
    u = impmid_driver(nt, nx, ts, xs, beta)
end;

println("Method time: $elapsed_time")

u_ref = zeros(Float128, nx)
open("../ref_sol/ref_sol_$nx.txt") do f 
    for (i, line) in enumerate(eachline(f))
        u_ref[i] = parse(Float128, line)
    end
end

dx = Float128(xs[2] - xs[1])
tmp = convert(Array{Float128}, u) - u_ref
l2_err = sqrt.(dx * dot(tmp, tmp))
println("L2 error with reference: $l2_err")