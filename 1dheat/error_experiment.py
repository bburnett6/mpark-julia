import subprocess as sp 
import os
import sys
import numpy as np 
#import matplotlib.pyplot as plt 
import json
import itertools
import multiprocessing as mp 

def run_prog(nt, nx, test_name, d_full, d_redu, corrections, n_runs=2):
	#dt computation. This will be the dt in the run
	ti = 0.0
	tf = 0.1
	timesteps = np.linspace(ti, tf, nt, endpoint=False)
	dt = timesteps[1] - timesteps[0]
	#on to the runs
	t = 0
	e = 0
	
	for i in range(n_runs):
		if corrections == 0:
			cmd = f'julia {test_name}.jl {d_full} {d_redu} {nt} {nx}'
		else:
			cmd = f'julia {test_name}-corrections.jl {d_full} {d_redu} {nt} {nx} {corrections}'
		out = sp.run(cmd.split(), capture_output=True)
		#print(out.stdout.split())
		lines = out.stdout.split()
		t = t + float(lines[2])
		e = e + float(lines[7])
	return t/n_runs, e/n_runs, dt 

"""
Run the full experiment for one of the tests
test_args - Tuple of shape ('test_name', nx, n_corrections)

Writes the results of the experiment inside the test directory to a 
json file
"""
def experiment(test_args):
	test_name = test_args[0]
	workdir = os.path.join('./', test_name)
	if not os.path.exists(workdir):
		print(f"experiment directory {test_name} doesn't exist. Returning...")
	os.chdir(workdir)

	d_types = ['Float128', 'Float64', 'Float32']
	nx = test_args[1]
	ncorr = test_args[2]
	combos = itertools.combinations_with_replacement(d_types, 2)
	nts = list(map(lambda p: 10**p, range(2, 7)))
	
	runs = []
	for d_full, d_redu in combos:
		run = {'redu': d_redu, 'full': d_full, 'errs': [], 'ts': [], 'dts': []}
		for nt in nts:
			print(f"Running {test_name} {d_full} {d_redu} with {ncorr} corrections for nt={nt}")
			t, e, dt = run_prog(nt, nx, test_name, d_full, d_redu, ncorr)
			print(f"Ave runtime: {t}")
			run['dts'].append(dt)
			run['errs'].append(e)
			run['ts'].append(t)
		runs.append(run)

	with open(f'runs_x86_{test_name}-{ncorr}_{nx}.json', 'w') as f:
		json.dump(runs, f)

	os.chdir('./..')

def main():
	#run the reference solution
	nx = sys.argv[1]
	if not os.path.isfile(f'./ref_sol/ref_sol_{nx}.txt'):
		os.chdir('./ref_sol')
		ref_nt = 10**7
		print(f"Running reference for {ref_nt}")
		out = sp.run(['julia', f'rk4.jl', 'Float128', 'Float128', f'{ref_nt}', f'{nx}', '1'], capture_output=True)
		os.chdir('./..')
	else:
		print("Reference solution exists. Proceeding on to tests.")
	#Use multiprocess to run all tests at once. 
	#Theoretically this shouldn't impact timing results because the 
	#tests are all serial. Requires >=4 hardware threads for this theory
	#to work
	#tests = [('impmid', nx, 0), ('impmid', nx, 1), ('impmid', nx, 2)]
	tests = [('impmid', nx, 1), ('impmid', nx, 2)]
	#with mp.Pool(processes=len(tests)) as pool:
	#	pool.map(experiment, tests)
	for test in tests:
		experiment(test)

if __name__ == '__main__':
	main()
