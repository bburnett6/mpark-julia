using Quadmath;
using LinearAlgebra;
using TimerOutputs;

##########################################
# Usage
#
# julia sdirk.jl FullType ReduType nt nx ncorr
# Example
# julia sdirk.jl Float64 Float32 100000 20 1
#
##########################################

# Types for mixed precision using type aliasing
const fullFloat = eval(Meta.parse(ARGS[1]));
const reduFloat = eval(Meta.parse(ARGS[2]));

# Initialization functions

function init_a(n::Integer, gamma::fullFloat)::Matrix{fullFloat}
    a = zeros(fullFloat, n, n)
    for i=2:n-1
        a[i, i+1] = gamma
        a[i, i] = fullFloat(-2.0) * gamma
        a[i, i-1] = gamma
    end
    a[1, 1] = fullFloat(1.0)
    a[n, n] = fullFloat(1.0)
    return a
end

function init_b(a::Matrix{fullFloat}, n::Integer, γ::fullFloat)::Matrix{reduFloat}
    b = Matrix{reduFloat}(I, n, n)
    b = b - γ * convert(Matrix{reduFloat}, a)
    return b 
end

function init_u(xs::Array{fullFloat}, n::Integer)::Array{fullFloat}
    #u = zeros(fullFloat, n)
    #for i=1:n-1
    #    u[i] = sin(fullFloat(2.0) * fullFloat(pi) * xs[i])
    #end
    u = sin.(fullFloat(2.0) * fullFloat(pi) * xs)
    u[n] = fullFloat(0.0)
    return u
end

function sdirk_driver(nt::Integer, nx::Integer, ts::Array{fullFloat}, xs::Array{fullFloat}, beta::fullFloat, ncorr::Integer)::Array{fullFloat}
    dt = ts[2] - ts[1]
    dx = xs[2] - xs[1]
    g = beta * dt / (dx * dx)
    γ = fullFloat((sqrt(3.0) + 3.0) / 6.0)
    u = init_u(xs, nx)
    unp1 = u
    a = init_a(nx, g)
    b = init_b(a, nx, γ)
    
    for (i, t) in enumerate(ts)
        #implicit stage 1
        y1 = b\convert(Array{reduFloat}, u)

        #correction step 1
        yk1 = convert(Array{fullFloat}, y1)
        for p=1:ncorr
            for k=2:nx-1
                yk1[k] = u[k] + g * γ * (yk1[k-1] + yk1[k+1] - fullFloat(2.0) * yk1[k]) 
            end
        end

        #implicit stage 2 
        y2 = b\convert(Array{reduFloat}, u + (fullFloat(1.0) - fullFloat(2.0)*γ) * (a * yk1))
        y2f = convert(Array{fullFloat}, y2)

        #correction step 2
        yk2 = convert(Array{fullFloat}, y2)
        for p=1:ncorr
            for k=2:nx-1
                yk2[k] = u[k] + (fullFloat(1.0) - fullFloat(2.0) * γ) * g * (yk1[k-1] + yk1[k+1] - fullFloat(2.0) * yk1[k]) + γ * g * (yk2[k-1] + yk2[k+1] - fullFloat(2.0) * yk2[k]) 
            end
        end

        #explicit step
        unp1[1] = u[1]
        for j=2:nx-1
            unp1[j] = u[j] + g/fullFloat(2.0) * (yk1[j-1] + yk1[j+1] - fullFloat(2.0) * yk1[j]) + g/fullFloat(2.0) * (yk2[j-1] + yk2[j+1] - fullFloat(2.0) * yk2[j])
        end
        unp1[nx] = u[nx]
        u = unp1
    end
    
    return u
end

####### Main

beta = fullFloat(1.0);
ti = fullFloat(0.0);
tf = fullFloat(0.1);
xi = fullFloat(0.0);
xf = fullFloat(1.0);

nt = parse(Int, ARGS[3]);
nx = parse(Int, ARGS[4]);
ncorr = parse(Int, ARGS[5]);

ts = collect(range(ti, tf, nt+1)[1:nt]); #Do not include end point! Use slicing achieve this because: https://github.com/JuliaLang/julia/issues/27097
xs = collect(range(xi, xf, nx)); # do include endpoint

#global tot_elapsed = 0.0
#nruns = 5
#for k=1:nruns
#    elapsed_time = @elapsed begin
#        u = sdirk_driver(nt, nx, ts, xs, beta)
#    end;
#    global tot_elapsed = tot_elapsed + elapsed_time 
#end
#tot_elapsed = tot_elapsed / nruns
#println("Ave elapsed time: $tot_elapsed")

elapsed_time = @elapsed begin
    u = sdirk_driver(nt, nx, ts, xs, beta, ncorr)
end;

println("Method time: $elapsed_time")

u_ref = zeros(Float128, nx)
open("../ref_sol/ref_sol_$nx.txt") do f 
    for (i, line) in enumerate(eachline(f))
        u_ref[i] = parse(Float128, line)
    end
end

dx = Float128(xs[2] - xs[1])
tmp = convert(Array{Float128}, u) - u_ref
l2_err = sqrt.(dx * dot(tmp, tmp))
println("L2 error with reference: $l2_err")