using Quadmath;
using LinearAlgebra;
using TimerOutputs;
using Profile;
using ProfileSVG;

##########################################
# Usage
#
# julia impmid.jl FullType ReduType nt nx ncorr
# Example
# julia impmid.jl Float64 Float32 100000 20 1
#
##########################################

# Types for mixed precision using type aliasing
const fullFloat = eval(Meta.parse(ARGS[1]));
const reduFloat = eval(Meta.parse(ARGS[2]));

# Initialization functions

function init_a(n::Integer, gamma::fullFloat)::Matrix{fullFloat}
    a = zeros(fullFloat, n, n)
    for i=2:n-1
        a[i, i+1] = gamma
        a[i, i] = fullFloat(-2.0) * gamma
        a[i, i-1] = gamma
    end
    a[1, 1] = fullFloat(1.0)
    a[n, n] = fullFloat(1.0)
    return a
end

function init_b(a::Matrix{fullFloat}, n::Integer, gamma::fullFloat)::Matrix{reduFloat}
    b = Matrix{reduFloat}(I, n, n)
    b = b - convert(Matrix{reduFloat}, a) / reduFloat(2.0)
    return b 
end

function init_u(xs::Array{fullFloat}, n::Integer)::Array{fullFloat}
    #u = zeros(fullFloat, n)
    #for i=1:n-1
    #    u[i] = sin(fullFloat(2.0) * fullFloat(pi) * xs[i])
    #end
    u = sin.(fullFloat(2.0) * fullFloat(pi) * xs)
    u[n] = fullFloat(0.0)
    return u
end

function impmid_driver(nt::Integer, nx::Integer, ts::Array{fullFloat}, xs::Array{fullFloat}, beta::fullFloat, ncorr::Integer)::Array{fullFloat}
    dt = ts[2] - ts[1]
    dx = xs[2] - xs[1]
    gamma = beta * dt / (dx * dx)
    u = init_u(xs, nx)
    unp1 = u
    a = init_a(nx, gamma)
    b = init_b(a, nx, gamma)
    
    for (i, t) in enumerate(ts)
        #implicit step
        y1 = b\convert(Array{reduFloat}, u)
        
        #correction step
        yk = convert(Array{fullFloat}, y1)
        for p=1:ncorr
            for k=2:nx-1
                yk[k] = u[k] + dt*beta/(fullFloat(2.0)*dx*dx) * (yk[k-1] + yk[k+1] - fullFloat(2.0) * yk[k]) 
            end
        end

        #explicit step
        unp1[1] = u[1]
        for j=2:nx-1
            unp1[j] = u[j] + dt*beta/(dx*dx) * (yk[j-1] + yk[j+1] - fullFloat(2.0) * yk[j])
        end
        unp1[nx] = u[nx]
        u = unp1
    end
    
    return u
end

####### Main

beta = fullFloat(1.0);
ti = fullFloat(0.0);
tf = fullFloat(0.1);
xi = fullFloat(0.0);
xf = fullFloat(1.0);

nt = parse(Int, ARGS[3]);
nx = parse(Int, ARGS[4]);
ncorr = parse(Int, ARGS[5]);

ts = collect(range(ti, tf, nt+1)[1:nt]); #Do not include end point! Use slicing achieve this because: https://github.com/JuliaLang/julia/issues/27097
xs = collect(range(xi, xf, nx)); # do include endpoint

@profile impmid_driver(nt, nx, ts, xs, beta, ncorr)
ProfileSVG.save(joinpath("profiles", "impmid-corr$(ncorr)-$(nx)_$(nt).svg"))