#!/bin/bash

python plot_results.py $1 $4 & PID1=$!
python plot_results.py $2 $4 & PID2=$!
python plot_results.py $3 $4 & PID3=$!

wait $PID1
wait $PID2
wait $PID3
