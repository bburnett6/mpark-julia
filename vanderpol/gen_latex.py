
import itertools
import matplotlib.pyplot as plt 
import matplotlib.cm as cm
import numpy as np 
import pandas as pd 
import argparse as ap 
import jinja2
import os 

def gen_table(alpha, method_name, ncorr, output_dir, debug):
	output_basedir = f"{output_dir}/{method_name}/corr-{ncorr}/alpha{alpha}"
	output_nocorr_basedir = f"{output_dir}/{method_name}/corr-0/alpha{alpha}"
	if not os.path.exists(f'{output_basedir}'):
		print(f'Outputs: {output_basedir} dont exist. Check args?')
		return

	#Get only the types that are available in the filenames.
	#getFloatType is used for sorting on the integer value of the float type (ie Float128 > Float64)
	getFloatType = lambda s: int(s.split('Float')[-1])
	#types is the set of all available types
	types = list(set(map(lambda s: s.split('_')[1], os.listdir(output_basedir))))
	#sort the types in reverse order based on the getFloatType function
	types.sort(reverse=True, key=getFloatType)
	#get all type combinations
	type_combos = list(itertools.combinations_with_replacement(types, 2))
	#similar process with getting the number of timesteps used
	nts = list(set(map(lambda s: int(s.split('.')[0].split('-')[-1]), os.listdir(output_basedir))))
	nts.sort()

	err_dt = {}
	runt_dt = {}
	names = []
	for d_full, d_redu in type_combos:
		name = f'{d_full}/{d_redu}'
		names.append(name)
		dts = []
		runtimes = []
		errors = []
		if d_full != d_redu:
			basedir = output_basedir 
		else:
			basedir = output_nocorr_basedir
		for nt in nts:
			dts.append(f'{1/(10 * 2**nt)}')
			filename = f'out_{d_full}_{d_redu}_nt-{nt}.txt'
			with open(os.path.join(basedir, filename), 'r') as f:
				#print(filename)
				output = f.read()
				#print(output)
				lines = output.splitlines()
				try:
					runtimes.append(float(lines[0].split()[-1]))
				except:
					runtimes.append(float("NaN"))
				try:
					errors.append(float(lines[1].split()[-1]))
				except:
					errors.append(float("NaN"))
		err_dt[name] = pd.Series(errors, index=dts)
		runt_dt[name] = pd.Series(runtimes, index=dts)

	#if not os.path.exists(f'./tables/{method_name}'):
	#	os.makedirs(f'./tables/{method_name}')
	#https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_markdown.html
	#with open(f'./tables/{method_name}/error_vs_dt.csv', 'w') as f:
	#	f.write(pd.DataFrame(err_dt).to_csv(index_label='dt'))
	#print(pd.DataFrame(err_dt))
	#with open(f'./tables/{method_name}/runtime_vs_dt.csv', 'w') as f:
	#	f.write(pd.DataFrame(runt_dt).to_csv(index_label='dt'))
	#print(pd.DataFrame(runt_dt))

	save_path = f'./{method_name}_latex/ncorr-{ncorr}/alpha{alpha}'
	img_type = "png"
	if not os.path.exists(f'{save_path}/tables'):
		os.makedirs(f'{save_path}/tables')
	if not os.path.exists(f'{save_path}/images'):
		os.makedirs(f'{save_path}/images')

	# Runtimes
	#tables
	df_rt = pd.DataFrame(runt_dt)
	if not debug:
		with open(f'{save_path}/tables/runtime1.tex', 'w') as f:
			f.write(df_rt.iloc[:,0:5].style.format("{:.4E}").to_latex())
		with open(f'{save_path}/tables/runtime2.tex', 'w') as f:
			f.write(df_rt.iloc[:,5:].style.format("{:.4E}").to_latex())
	#plots
	colors = cm.turbo(np.linspace(0, 1, df_rt.shape[1]))
	xs = np.array(list(map(lambda s: float(s), df_rt.index.values)))
	ys = np.array(df_rt[df_rt.columns[:]])
	for i in range(df_rt.shape[1]):
		plt.loglog(xs, ys[:,i], label=names[i], color=colors[i])
		plt.scatter(xs, ys[:,i], color=colors[i], s=(df_rt.shape[1]-i)*20)
	plt.xlabel('dt')
	plt.ylabel('runtime')
	#plt.ylim(top=1e-3)
	#plt.xticks(xs)
	plt.grid()
	plt.legend(prop={'size': 8})
	plt.title(f'{method_name.upper()}')
	if debug:
		plt.show()
	else:
		plt.savefig(f'{save_path}/images/runtime_v_dt.{img_type}', format=f'{img_type}', dpi=1000)
	plt.clf()

	#Errors
	#tables
	df_err = pd.DataFrame(err_dt)
	if not debug:
		with open(f'{save_path}/tables/err1.tex', 'w') as f:
			f.write(df_err.iloc[:,0:5].style.format("{:.4E}").to_latex())
		with open(f'{save_path}/tables/err2.tex', 'w') as f:
			f.write(df_err.iloc[:,5:].style.format("{:.4E}").to_latex())
	#plots
	colors = cm.turbo(np.linspace(0, 1, df_err.shape[1]))
	xs = np.array(list(map(lambda s: float(s), df_err.index.values)))
	ys = np.array(df_err[df_err.columns[:]])
	for i in range(df_err.shape[1]):
		plt.loglog(xs, ys[:,i], label=names[i], color=colors[i])
		plt.scatter(xs, ys[:,i], color=colors[i], s=(df_err.shape[1]-i)*20)
	plt.xlabel('dt')
	plt.ylabel('error')
	plt.ylim(top=1e2, bottom=1e-24)
	#plt.xticks(xs)
	#plt.xlim(left=xs[0], right=xs[-1])
	plt.grid()
	plt.legend(prop={'size': 8})
	plt.title(f'{method_name.upper()}')
	if debug:
		plt.show()
	else:
		plt.savefig(f'{save_path}/images/err_v_dt.{img_type}', format=f'{img_type}', dpi=1000)
	plt.clf()

	#Efficiency
	#plots
	colors = cm.turbo(np.linspace(0, 1, df_rt.shape[1]))
	xs = np.array(df_rt[df_rt.columns[:]])
	ys = np.array(df_err[df_err.columns[:]])
	for i in range(df_rt.shape[1]):
		plt.loglog(xs[:,i], ys[:,i], label=names[i], color=colors[i])
		plt.scatter(xs[:,i], ys[:,i], color=colors[i], s=(df_rt.shape[1]-i)*20)
	plt.xlabel('runtime')
	plt.ylabel('error')
	#plt.ylim(top=1e-3)
	#plt.xticks(xs)
	#plt.xlim(left=xs[0], right=xs[-1])
	plt.grid()
	plt.legend(prop={'size': 8})
	plt.title(f'{method_name.upper()}')
	if debug:
		plt.show()
	else:
		plt.savefig(f'{save_path}/images/err_v_runtime.{img_type}', format=f'{img_type}', dpi=1000)
	plt.clf()

class TexData:
	def __init__(self, ncorr, alpha):
		self.ncorr = ncorr
		self.alpha = alpha

class TexHeaders:
	def __init__(self, title, author):
		self.author = author 
		self.title = title 

def gen_latex(ncorr, alphas, method_name):
	#thanks: http://eosrei.net/articles/2015/11/latex-templates-python-and-jinja2-generate-pdfs
	latex_env = jinja2.Environment(
		block_start_string = '\BLOCK{',
		block_end_string = '}',
		variable_start_string = '\VAR{',
		variable_end_string = '}',
		comment_start_string = '\#{',
		comment_end_string = '}',
		line_statement_prefix = '%%',
		line_comment_prefix = '%#',
		trim_blocks = True,
		autoescape = False,
		loader = jinja2.FileSystemLoader(os.path.abspath('./latex-templates'))
		)
	template = latex_env.get_template('mpark_latex.tex')

	td = TexData(ncorr, alphas)
	th = TexHeaders(f"{method_name} using Central Difference", "Ben Burnett")

	save_path = f'./{method_name}_latex/{method_name}.tex'
	with open(save_path, 'w') as f: 
		f.write(template.render(tex_headers=th, tex_data=td))

if __name__ == '__main__':
	parser = ap.ArgumentParser(description='Plot the output of runs of the Van der Pol equation for a given alpha and number of corrections')
	parser.add_argument(
		'--method', 
		dest='method', 
		default="novel-4s3pA",
		help='Name of method to use'
		)
	parser.add_argument(
		'--outdir', 
		dest='outdir', 
		default="output_threads-24",
		help='Directory where outputs are stored'
		)
	args = parser.parse_args()
	
	#get all corrections
	ncorrs = list(set(map(lambda s: int(s.split('r-')[-1]), os.listdir(f'{args.outdir}/{args.method}'))))
	ncorrs.sort()

	#get all nxs. Should be same for all m 
	alphas = list(set(map(lambda s: s.split('pha')[-1], os.listdir(f'{args.outdir}/{args.method}/corr-{ncorrs[0]}'))))
	alphas.sort(key=lambda x: float(x)) #sort alphas by numerical value
	
	for ncorr in ncorrs:
		print(ncorr)
		for alpha in alphas:
			print('\t', alpha)
			gen_table(alpha, args.method, ncorr, args.outdir, debug=False)

	#generate tex document
	gen_latex(ncorrs, alphas, args.method)
	