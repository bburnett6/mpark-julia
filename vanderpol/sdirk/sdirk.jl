
using Quadmath;
#using NLsolve;
#using BenchmarkTools;
using LinearAlgebra;
using TimerOutputs;

##########################################
# Usage
#
# julia -t n_threads sdirk.jl FullType ReduType nt alpha
# Example
# julia -t 4 sdirk.jl Float64 Float32 100000 1.0
#
##########################################

#BLAS threading
BLAS.set_num_threads(Threads.nthreads())

const FullFloat = eval(Meta.parse(ARGS[1]));
const ReduFloat = eval(Meta.parse(ARGS[2]));

############ Functions

function vanderpol_sdirk_F!(fn::Array{ReduFloat}, y::Array{ReduFloat}, u::Array{ReduFloat}, dt::ReduFloat, α::ReduFloat)
    fn .= ReduFloat(0.0)

    fn[1] = u[1] + dt * (y[2]) - y[1]
    fn[2] = u[2] + dt * (α * (FullFloat(1.0) - y[1]*y[1]) * y[2] - y[1]) - y[2]

end

function vanderpol_sdirk_J!(J::Matrix{ReduFloat}, y::Array{ReduFloat}, dt::ReduFloat, α::ReduFloat)
    J[:, :] .= ReduFloat(0.0)

    J[1, 1] = ReduFloat(-1.0)
    J[1, 2] = dt
    J[2, 1] = dt * (α * y[1] * y[2] - ReduFloat(1.0))
    J[2, 2] = dt * (α * (ReduFloat(1.0) - y[1]*y[1])) - ReduFloat(1.0)

end

#=
My newtons method. Takes in inital guess x0 and mutates it to the solution over
max_iter iterations. Fn and Jn are preallocated placeholders for the iteration
computations. 
=#
function my_newtons!(f!, j!, x0::Array{ReduFloat}, Fn::Array{ReduFloat}, Jn::Matrix{ReduFloat})::Array{ReduFloat}
    tol = eps(ReduFloat) * .5e1; #Tolerance of the solver slightly larger than machine epsilon for that precision
    max_iter = 20; #maximum number of iterations for the solver 

    #println("starting newtons")
    for i=1:max_iter
        f!(Fn, x0)
        if norm(Fn) < tol 
            #println("iter: $i, tol: $(norm(Fn))")
            return x0
        end

        j!(Jn, x0)

        #x0 = x0 - inv(Jn) * Fn;
        x0 = x0 - Jn \ Fn;
        #println(norm(Fn))
    end
    return x0
end

function init_u()::Array{FullFloat}
    u = zeros(FullFloat, 2)
    u[1] = FullFloat(2.0)
    u[2] = FullFloat(0.0)
    return u
end

function sdirk_driver(nt::Integer, tf::FullFloat, α::FullFloat, ts::Array{FullFloat})::Array{FullFloat}
    dt = ts[2] - ts[1]
    t_total = FullFloat(ts[1])
    u = init_u()
    unp1 = copy(u)
    fn = zeros(ReduFloat, 2)
    jn = zeros(ReduFloat, 2, 2)
    γ = (sqrt(3.0) + 3.0) / 6.0
    #jacobian doesn't change each time step so leave it out of the loop
    j!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = vanderpol_sdirk_J!(J, y, ReduFloat(γ*dt), ReduFloat(α))
    
    for (i, t) in enumerate(ts)
        #Make sure final time is reached correctly
        if i == nt 
            dt = tf - t_total
        end
        #implicit step 1
        tmp_u = convert(Array{ReduFloat}, u)
        #implicit function changes with u, so redefine each time step
        f1!(F::Array{ReduFloat}, y::Array{ReduFloat}) = vanderpol_sdirk_F!(F, y, tmp_u, ReduFloat(γ*dt), ReduFloat(α))
        #y1 = nlsolve(f!, j!, tmp_u).zero #solve the nonlinear system f! using initial guess u
        y1 = my_newtons!(f1!, j!, tmp_u, fn, jn)

        #implicit step 2
        tmp_u2 = zeros(ReduFloat, 2)
        tmp_u2[1] = ReduFloat(u[1]) + (ReduFloat(1.0) - ReduFloat(2.0*γ)) * ReduFloat(dt) * (y1[2])
        tmp_u2[2] = ReduFloat(u[2]) + (ReduFloat(1.0) - ReduFloat(2.0*γ)) * ReduFloat(dt) * (ReduFloat(α) * (ReduFloat(1.0) - y1[1]*y1[1]) * y1[2] - y1[1])
        #implicit function changes with u, so redefine each time step
        f2!(F::Array{ReduFloat}, y::Array{ReduFloat}) = vanderpol_sdirk_F!(F, y, tmp_u2, ReduFloat(γ*dt), ReduFloat(α))
        #y1 = nlsolve(f!, j!, tmp_u).zero #solve the nonlinear system f! using initial guess u
        y2 = my_newtons!(f2!, j!, tmp_u2, fn, jn)
        
        #explicit step
        y1f = convert(Array{FullFloat}, y1)
        y2f = convert(Array{FullFloat}, y2)
        u[1] = u[1] + FullFloat(0.5)*dt * y1f[2] + FullFloat(0.5)*dt * y2f[2]
        u[2] = u[2] + FullFloat(0.5)*dt * (α * (FullFloat(1.0) - y1f[1]*y1f[1]) * y1f[2] - y1f[1]) + FullFloat(0.5)*dt * (α * (FullFloat(1.0) - y2f[1]*y2f[1]) * y2f[2] - y2f[1])
        t_total += dt 
    end
    
    return u
end

######### Main

#initial conditions
ti = FullFloat(0.0);
tf = FullFloat(1.0);

nt = parse(Int, ARGS[3]);
alpha = parse(FullFloat, ARGS[4]);

ts = collect(range(ti, tf, nt+1)[1:nt]);

u = sdirk_driver(nt, tf, alpha, ts)

global tot_elapsed = 0.0
nruns = 4
for k=1:nruns
    elapsed_time = @elapsed begin
        uel = sdirk_driver(nt, tf, alpha, ts)
    end;
    #println("One done in: $elapsed_time")
    global tot_elapsed = tot_elapsed + elapsed_time 
end
tot_elapsed = tot_elapsed / nruns
println("Ave elapsed time: $tot_elapsed")

#=
elapsed_time = @elapsed begin
    u = sdirk_driver(nt, tf, alpha, ts)
end;
println("Method Time: $elapsed_time")
=#

u_ref = zeros(Float128, 2)
open("./ref_sol/ref_sol_a$(ARGS[4]).txt") do f 
    for (i, line) in enumerate(eachline(f))
        u_ref[i] = parse(Float128, line)
    end
end

tmp = convert(Array{Float128}, u) - u_ref
l2_err = sqrt.(dot(tmp, tmp))
println("L2 error with reference: $l2_err")