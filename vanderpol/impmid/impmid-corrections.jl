
using Quadmath;
#using NLsolve;
#using BenchmarkTools;
using LinearAlgebra;
using TimerOutputs;

##########################################
# Usage
#
# julia -t n_threads impmid-corrections.jl FullType ReduType nt alpha ncorr
# Example
# julia -t 4 impmid-corrections.jl Float64 Float32 100000 1.0 1
#
##########################################

#BLAS threading
BLAS.set_num_threads(Threads.nthreads())

const FullFloat = eval(Meta.parse(ARGS[1]));
const ReduFloat = eval(Meta.parse(ARGS[2]));

############ Functions

function vanderpol_imr_F!(fn::Array{ReduFloat}, y::Array{ReduFloat}, u::Array{ReduFloat}, dt::ReduFloat, α::ReduFloat)
    fn .= ReduFloat(0.0)

    fn[1] = u[1] + dt * (y[2]) - y[1]
    fn[2] = u[2] + dt * (α * (FullFloat(1.0) - y[1]*y[1]) * y[2] - y[1]) - y[2]

end

function vanderpol_imr_J!(J::Matrix{ReduFloat}, y::Array{ReduFloat}, dt::ReduFloat, α::ReduFloat)
    J[:, :] .= ReduFloat(0.0)

    J[1, 1] = ReduFloat(-1.0)
    J[1, 2] = dt
    J[2, 1] = dt * (α * y[1] * y[2] - ReduFloat(1.0))
    J[2, 2] = dt * (α * (ReduFloat(1.0) - y[1]*y[1])) - ReduFloat(1.0)

end

#=
My newtons method. Takes in inital guess x0 and mutates it to the solution over
max_iter iterations. Fn and Jn are preallocated placeholders for the iteration
computations. 
=#
function my_newtons!(f!, j!, x0::Array{ReduFloat}, Fn::Array{ReduFloat}, Jn::Matrix{ReduFloat})::Array{ReduFloat}
    tol = eps(ReduFloat) * .5e1; #Tolerance of the solver slightly larger than machine epsilon for that precision
    max_iter = 20; #maximum number of iterations for the solver 

    #println("starting newtons")
    for i=1:max_iter
        f!(Fn, x0)
        if norm(Fn) < tol 
            #println("iter: $i, tol: $(norm(Fn))")
            return x0
        end

        j!(Jn, x0)

        #x0 = x0 - inv(Jn) * Fn;
        x0 = x0 - Jn \ Fn;
        #println(norm(Fn))
    end
    return x0
end

function init_u()::Array{FullFloat}
    u = zeros(FullFloat, 2)
    u[1] = FullFloat(2.0)
    u[2] = FullFloat(0.0)
    return u
end

function impmid_driver(nt::Integer, tf::FullFloat, α::FullFloat, ts::Array{FullFloat}, ncorr::Integer)::Array{FullFloat}
    dt = ts[2] - ts[1]
    t_total = FullFloat(ts[1])
    u = init_u()
    unp1 = copy(u)
    fn = zeros(ReduFloat, 2)
    jn = zeros(ReduFloat, 2, 2)
    #jacobian doesn't change each time step so leave it out of the loop
    j!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = vanderpol_imr_J!(J, y, ReduFloat(0.5*dt), ReduFloat(α))
    
    for (i, t) in enumerate(ts)
        #Make sure final time is reached correctly
        if i == nt 
            dt = tf - t_total
        end

        #implicit step
        tmp_u = convert(Array{ReduFloat}, u)
        #implicit function changes with u, so redefine each time step
        f!(F::Array{ReduFloat}, y::Array{ReduFloat}) = vanderpol_imr_F!(F, y, tmp_u, ReduFloat(0.5*dt), ReduFloat(α))
        #y1 = nlsolve(f!, j!, tmp_u).zero #solve the nonlinear system f! using initial guess u
        y1 = my_newtons!(f!, j!, tmp_u, fn, jn)

        #correction step
        y1k = convert(Array{FullFloat}, y1)
        for k=1:ncorr
            y1k[1] = u[1] + FullFloat(0.5)*dt * (y1k[2])
            y1k[2] = u[2] + FullFloat(0.5)*dt * (α * (FullFloat(1.0) - y1k[1]*y1k[1]) * y1k[2] - y1k[1])
        end
        
        #explicit step
        u[1] = u[1] + dt * y1k[2]
        u[2] = u[2] + dt * (α * (FullFloat(1.0) - y1k[1]*y1k[1]) * y1k[2] - y1k[1])
        t_total += dt 
    end
    
    return u
end

######### Main

#initial conditions
ti = FullFloat(0.0);
tf = FullFloat(1.0);

nt = parse(Int, ARGS[3]);
alpha = parse(FullFloat, ARGS[4])
ncorr = parse(Int, ARGS[5])

ts = collect(range(ti, tf, nt+1)[1:nt]);

u = impmid_driver(nt, tf, alpha, ts, ncorr)

global tot_elapsed = 0.0
nruns = 4
for k=1:nruns
    elapsed_time = @elapsed begin
        uel = impmid_driver(nt, tf, alpha, ts, ncorr)
    end;
    #println("One done in: $elapsed_time")
    global tot_elapsed = tot_elapsed + elapsed_time 
end
tot_elapsed = tot_elapsed / nruns
println("Ave elapsed time: $tot_elapsed")

#=
elapsed_time = @elapsed begin
    u = impmid_driver(nt, tf, alpha, ts, ncorr)
end;
println("Method Time: $elapsed_time")
=#

u_ref = zeros(Float128, 2)
open("./ref_sol/ref_sol_a$(ARGS[4]).txt") do f 
    for (i, line) in enumerate(eachline(f))
        u_ref[i] = parse(Float128, line)
    end
end

tmp = convert(Array{Float128}, u) - u_ref
l2_err = sqrt.(dot(tmp, tmp))
println("L2 error with reference: $l2_err")
