#!/bin/bash

{% for option in options %}#SBATCH --{{ option.name }}={{ option.value }}
{% endfor %}
#Load required modules

module load julia 

# Run Julia looped for values of alpha
# julia -t n_threads impmid.jl FullType ReduType nt alpha
for ALPHA in 0.001 0.01 0.1 1.0 10.0 100.0 1000.0 100000000.0
do
	julia -t {{ cmd.n_threads }} \
		{{ cmd.filename }} \
		{{ cmd.fulltype }} \
		{{ cmd.redutype }} \
		$((10 ** {{ cmd.nt }} )) \
		$ALPHA {% if cmd.is_corr %}\
		{{ cmd.ncorr }}
		{% endif %}
done