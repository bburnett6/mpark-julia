using Quadmath;
using DelimitedFiles;
using LinearAlgebra;
using TimerOutputs;

##########################################
# Usage
#
# julia rk4.jl FullType ReduType nt alpha write_ref
# Example:
# julia rk4.jl Float128 Float128 1000000 1.0 1
#
##########################################

# Types for mixed precision using type aliasing
const FullFloat = eval(Meta.parse(ARGS[1]));
const ReduFloat = eval(Meta.parse(ARGS[2]));

function fi(y::Array{FullFloat}, dt::FullFloat, α::FullFloat)::Array{FullFloat}
    fn = zeros(FullFloat, 2)

    fn[1] = y[2]
    fn[2] = α * (FullFloat(1.0) - y[1]*y[1]) * y[2] - y[1]

    return fn
end

function init_u()::Array{FullFloat}
    u = zeros(FullFloat, 2)
    u[1] = FullFloat(2.0)
    u[2] = FullFloat(0.0)
    return u
end

function rk4_driver(nt::Integer, tf::FullFloat, ts::Array{FullFloat}, α::FullFloat)::Array{FullFloat}
    dt = ts[2] - ts[1]
    u = init_u()
    t_total = FullFloat(ts[1])
    
    for (i, t) in enumerate(ts)
        if i == nt
            dt = tf - t_total
        end
        k1 = fi(u, t, α)
        tmp = u + dt/FullFloat(2.0) * k1 
        k2 = fi(tmp, t + dt/FullFloat(2.0), α)
        tmp = u + dt/FullFloat(2.0) * k2 
        k3 = fi(tmp, t + dt/FullFloat(2.0), α)
        tmp = u + dt * k3 
        k4 = fi(tmp, t + dt, α)

        u = u + FullFloat(1.0)/FullFloat(6.0) * dt * (k1 + FullFloat(2.0)*k2 + FullFloat(2.0)*k3 + k4)
        t_total += dt 
    end
    
    return u
end

####### Main

#initial conditions
ti = FullFloat(0.0);
tf = FullFloat(1.0);

nt = parse(Int, ARGS[3]);
alpha = parse(FullFloat, ARGS[4])
write_ref = parse(Int, ARGS[5]);

ts = collect(range(ti, tf, nt+1)[1:nt]);

elapsed_time = @elapsed begin
    u = rk4_driver(nt, tf, ts, alpha)
end;

println("Method time: $elapsed_time")

if write_ref == 1
    open("ref_sol_a$(ARGS[4]).txt", "w") do io 
        writedlm(io, u)
    end 
end 

#This is broken. Tryparse fails and is unable to read float128
#u_ref = readdlm("ref_sol_$nx.txt", '\t', FullFloat, '\n')
u_ref = zeros(FullFloat, 2)
open("ref_sol_a$(ARGS[4]).txt") do f 
    for (i, line) in enumerate(eachline(f))
        u_ref[i] = parse(FullFloat, line)
    end
end

tmp = u - u_ref
l2_err = sqrt.(dot(tmp, tmp))
println("L2 error with reference: $l2_err")