#!/bin/bash

set -x

NT=100000000

for ALPHA in 0.001 0.01 0.1 1.0 10.0 100.0 1000.0
do
	julia rk4.jl Float128 Float128 $NT $ALPHA 1
done
