
import itertools
import numpy as np 
import pandas as pd 
import argparse as ap 
import os 

def gen_table(nx, method_name, ncorr, nthreads):
	output_basedir = f"output_threads-{nthreads}/{method_name}/corr-{ncorr}"
	nocorr_basedir = f"output_threads-{nthreads}/{method_name}/corr-0"
	if not os.path.exists(f'{output_basedir}'):
		print('Outputs dont exist. Check args?')
		return

	#Get only the types that are available in the filenames.
	#getFloatType is used for sorting on the integer value of the float type (ie Float128 > Float64)
	getFloatType = lambda s: int(s.split('Float')[-1])
	#types is the set of all available types
	types = list(set(map(lambda s: s.split('_')[1], os.listdir(output_basedir))))
	#sort the types in reverse order based on the getFloatType function
	types.sort(reverse=True, key=getFloatType)
	#get all type combinations
	type_combos = list(itertools.combinations_with_replacement(types, 2))
	#similar process with getting the number of timesteps used
	nts = list(set(map(lambda s: int(s.split('.')[0].split('-')[-1]), os.listdir(output_basedir))))
	nts.sort()

	err_dt = {}
	runt_dt = {}
	for d_full, d_redu in type_combos:
		name = f'{d_full}/{d_redu}'
		dts = []
		runtimes = []
		errors = []
		for nt in nts:
			dts.append(f'{1/10**nt}')
			filename = f'out_{d_full}_{d_redu}_nt-{nt}.txt'
			if d_full == d_redu:
				with open(os.path.join(nocorr_basedir, filename), 'r') as f:
					#print(filename)
					output = f.read()
					#print(output)
					lines = output.splitlines()
					runtimes.append(float(lines[0].split()[-1]))
					errors.append(float(lines[1].split()[-1]))
			else:
				with open(os.path.join(output_basedir, filename), 'r') as f:
					#print(filename)
					output = f.read()
					#print(output)
					lines = output.splitlines()
					runtimes.append(float(lines[0].split()[-1]))
					errors.append(float(lines[1].split()[-1]))
		err_dt[name] = pd.Series(errors, index=dts)
		runt_dt[name] = pd.Series(runtimes, index=dts)

	if not os.path.exists(f'./tables/{method_name}/corrections{ncorr}'):
		os.makedirs(f'./tables/{method_name}/corrections{ncorr}')
	#https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_markdown.html
	with open(f'./tables/{method_name}/corrections{ncorr}/error_vs_dt.csv', 'w') as f:
		f.write(pd.DataFrame(err_dt).to_csv(index_label='dt'))
	#print(pd.DataFrame(err_dt))
	with open(f'./tables/{method_name}/corrections{ncorr}/runtime_vs_dt.csv', 'w') as f:
		f.write(pd.DataFrame(runt_dt).to_csv(index_label='dt'))
	#print(pd.DataFrame(runt_dt))


if __name__ == '__main__':
	parser = ap.ArgumentParser(description='Plot the output of runs of the Burgers equation for a given nx and number of corrections')
	parser.add_argument(
		'--nx',
		dest='nx',
		default='50',
		help='Number of spacial points'
		)
	parser.add_argument(
		'--ncorr',
		dest='ncorr',
		default='0',
		help='Number of corrections'
		)
	parser.add_argument(
		'-t', 
		dest='threads', 
		default="8",
		help='Number of threads used by BLAS'
		)
	parser.add_argument(
		'-m', 
		dest='method', 
		default="impmid",
		help='Name of the method used to compute solution'
		)
	args = parser.parse_args()

	gen_table(args.nx, args.method, args.ncorr, args.threads)