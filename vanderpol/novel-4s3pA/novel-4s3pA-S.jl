
using Quadmath;
#using BenchmarkTools;
using LinearAlgebra;
using TimerOutputs;

##########################################
# Usage
#
# julia -t n_threads novel.jl FullType ReduType nt alpha
# Example
# julia -t 4 novel.jl Float64 Float32 100000 1.0
#
##########################################

#BLAS threading
BLAS.set_num_threads(Threads.nthreads())

const FullFloat = eval(Meta.parse(ARGS[1]));
const ReduFloat = eval(Meta.parse(ARGS[2]));

############ Functions

#=
My newtons method. Takes in inital guess x0 and mutates it to the solution over
max_iter iterations. Fn and Jn are preallocated placeholders for the iteration
computations. 
=#
function my_newtons!(dt::ReduFloat, α::ReduFloat, x0::Array{ReduFloat}, u::Array{ReduFloat})::Array{ReduFloat}
    tol = eps(ReduFloat) * .5e1; #Tolerance of the solver slightly larger than machine epsilon for that precision
    max_iter = 20; #maximum number of iterations for the solver 

    #println("starting newtons")
    for i=1:max_iter
        Fn = zeros(ReduFloat, 2)
        Fn[1] = u[1] + dt * (x0[2]) - x0[1]
        Fn[2] = u[2] + dt * (α * (FullFloat(1.0) - x0[1]*x0[1]) * x0[2] - x0[1]) - x0[2]

        if norm(Fn) < tol 
            #println("iter: $i, tol: $(norm(Fn))")
            return x0
        end

        Jn = zeros(ReduFloat, 2, 2)
        Jn[1, 1] = ReduFloat(-1.0)
        Jn[1, 2] = dt
        Jn[2, 1] = dt * (α * x0[1] * x0[2] - ReduFloat(1.0))
        Jn[2, 2] = dt * (α * (ReduFloat(1.0) - x0[1]*x0[1])) - ReduFloat(1.0)

        #x0 = x0 - inv(Jn) * Fn;
        x0 = x0 - Jn \ Fn;
        #println(norm(Fn))
    end
    return x0
end

function init_u()::Array{FullFloat}
    u = zeros(FullFloat, 2)
    u[1] = FullFloat(2.0)
    u[2] = FullFloat(0.0)
    return u
end

function rhs_f(y::Array{FullFloat})::Array{FullFloat}
    u = zeros(FullFloat, 2)
    u[1] = y[2]
    u[2] = y[2] * (1 - y[1]*y[1]) - y[1]
    return u 
end

function rhs_r(y::Array{ReduFloat})::Array{ReduFloat}
    u = zeros(ReduFloat, 2)
    u[1] = y[2]
    u[2] = y[2] * (1 - y[1]*y[1]) - y[1]
    return u 
end

function novel_driver(nt::Integer, tf::FullFloat, α::FullFloat, ts::Array{FullFloat})::Array{FullFloat}
    dt = ts[2] - ts[1]
    t_total = FullFloat(ts[1])
    u = init_u()
    unp1 = copy(u)
    fn = zeros(ReduFloat, 2)
    jn = zeros(ReduFloat, 2, 2)

    #Butcher coefficient matrix 
    butcherA = zeros(FullFloat, 4, 4)
    butcherA[2, 1] = 0.211324865405187
    butcherA[3, 1] = 0.709495523817170
    butcherA[3, 2] = -0.865314250619423
    butcherA[4, 1] = 0.705123240545107
    butcherA[4, 2] = 0.943370088535775
    butcherA[4, 3] = -0.859818194486069

    butcherAe = zeros(FullFloat, 4, 4)
    butcherAe[1, 1] = 0.788675134594813
    butcherAe[3, 1] = 0.051944240459852
    butcherAe[3, 3] = 0.788675134594813

    butcherB = zeros(FullFloat, 4)
    butcherB[2] = 0.5
    butcherB[4] = 0.5

    for (i, t) in enumerate(ts)
        #Make sure final time is reached correctly
        if i == nt 
            dt = tf - t_total
        end
        ur = ReduFloat.(u)
        #Stage 1 (implicit)
        tmp_u = convert(Array{ReduFloat}, u)
        #implicit function changes with u, so redefine each time step
        y1 = my_newtons!(dt * butcherAe[1, 1], α, ur, tmp_u)
        y1f = FullFloat.(y1)

        #Stage 2 (explicit)
        y2f = u[:] .+ (dt * butcherA[2, 1]) .* rhs_f(y1f)

        #Stage 3 (implicit)
        tmp_u3 = zeros(FullFloat, 2)
        tmp_u3[:] = u[:] .+ (dt * butcherA[3, 1]) .* rhs_f(y1f) 
        tmp_u3[:] += (dt * butcherA[3, 2]) .* rhs_f(y2f)
        tmp_u3[:] += (dt * butcherAe[3, 1]) .* FullFloat.(rhs_r(y1))
        #implicit function changes with u, so redefine each time step
        y3 = my_newtons!(dt * butcherAe[3, 3], α, ur, tmp_u3)
        y3f = FullFloat.(y3)

        #Stage 4 (explicit)
        y4f = u[:] .+ (dt * butcherA[4, 1]) .* rhs_f(y1f)
        y4f[:] += (dt * butcherA[4, 2]) .* rhs_f(y2f)
        y4f[:] += (dt * butcherA[4, 3]) .* rhs_f(y3f)
        
        #update stage
        unp1[:] = u[:] .+ (dt * butcherB[2]) .* rhs_f(y2f)
        unp1[:] += (dt * butcherB[4]) .* rhs_f(y4f)

        u[:] = unp1[:]
        t_total += dt 
    end
    
    return u
end

######### Main

#initial conditions
ti = FullFloat(0.0);
tf = FullFloat(1.0);

nt = parse(Int, ARGS[3]);
alpha = parse(FullFloat, ARGS[4]);

ts = collect(range(ti, tf, nt+1)[1:nt]);

u = novel_driver(nt, tf, alpha, ts)

global tot_elapsed = 0.0
nruns = 4
for k=1:nruns
    elapsed_time = @elapsed begin
        uel = novel_driver(nt, tf, alpha, ts)
    end;
    #println("One done in: $elapsed_time")
    global tot_elapsed = tot_elapsed + elapsed_time 
end
tot_elapsed = tot_elapsed / nruns
println("Ave elapsed time: $tot_elapsed")

#=
elapsed_time = @elapsed begin
    u = novel_driver(nt, tf, alpha, ts)
end;
println("Method Time: $elapsed_time")
=#

u_ref = zeros(Float128, 2)
open("./ref_sol/ref_sol_a$(ARGS[4]).txt") do f 
    for (i, line) in enumerate(eachline(f))
        u_ref[i] = parse(Float128, line)
    end
end

tmp = convert(Array{Float128}, u) - u_ref
l2_err = sqrt.(dot(tmp, tmp))
println("L2 error with reference: $l2_err")