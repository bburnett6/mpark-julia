
using Quadmath;
#using BenchmarkTools;
using LinearAlgebra;
using TimerOutputs;

##########################################
# Usage
#
# julia -t n_threads novel.jl FullType ReduType nt alpha
# Example
# julia -t 4 novel.jl Float64 Float32 100000 1.0
#
##########################################

#BLAS threading
BLAS.set_num_threads(Threads.nthreads())

const FullFloat = eval(Meta.parse(ARGS[1]));
const ReduFloat = eval(Meta.parse(ARGS[2]));

############ Functions

function vanderpol_novel_F!(fn::Array{ReduFloat}, y::Array{ReduFloat}, u::Array{ReduFloat}, dt::ReduFloat, α::ReduFloat)
    fn .= ReduFloat(0.0)

    fn[1] = u[1] + dt * (y[2]) - y[1]
    fn[2] = u[2] + dt * (α * (FullFloat(1.0) - y[1]*y[1]) * y[2] - y[1]) - y[2]

end

function vanderpol_novel_J!(J::Matrix{ReduFloat}, y::Array{ReduFloat}, dt::ReduFloat, α::ReduFloat)
    J[:, :] .= ReduFloat(0.0)

    J[1, 1] = ReduFloat(-1.0)
    J[1, 2] = dt
    J[2, 1] = dt * (α * y[1] * y[2] - ReduFloat(1.0))
    J[2, 2] = dt * (α * (ReduFloat(1.0) - y[1]*y[1])) - ReduFloat(1.0)

end

#=
My newtons method. Takes in inital guess x0 and mutates it to the solution over
max_iter iterations. Fn and Jn are preallocated placeholders for the iteration
computations. 
=#
function my_newtons!(f!, j!, x0::Array{ReduFloat}, Fn::Array{ReduFloat}, Jn::Matrix{ReduFloat})::Array{ReduFloat}
    tol = eps(ReduFloat) * .5e1; #Tolerance of the solver slightly larger than machine epsilon for that precision
    max_iter = 20; #maximum number of iterations for the solver 

    #println("starting newtons")
    for i=1:max_iter
        f!(Fn, x0)
        if norm(Fn) < tol 
            #println("iter: $i, tol: $(norm(Fn))")
            return x0
        end

        j!(Jn, x0)

        #x0 = x0 - inv(Jn) * Fn;
        x0 = x0 - Jn \ Fn;
        #println(norm(Fn))
    end
    return x0
end

function init_u()::Array{FullFloat}
    u = zeros(FullFloat, 2)
    u[1] = FullFloat(2.0)
    u[2] = FullFloat(0.0)
    return u
end

function rhs_f(y::Array{FullFloat}, alpha::FullFloat)::Array{FullFloat}
    u = zeros(FullFloat, 2)
    u[1] = y[2]
    u[2] = alpha * y[2] * (1 - y[1]*y[1]) - y[1]
    return u 
end

function rhs_r(y::Array{ReduFloat}, alpha::ReduFloat)::Array{ReduFloat}
    u = zeros(ReduFloat, 2)
    u[1] = y[2]
    u[2] = alpha * y[2] * (1 - y[1]*y[1]) - y[1]
    return u 
end

function novel_driver(nt::Integer, tf::FullFloat, α::FullFloat, ts::Array{FullFloat})::Array{FullFloat}
    dt = ts[2] - ts[1]
    t_total = FullFloat(ts[1])
    u = init_u()
    unp1 = copy(u)
    α_r = ReduFloat(α)
    fn = zeros(ReduFloat, 2)
    jn = zeros(ReduFloat, 2, 2)

    #Butcher coefficient matrix 
    butcherA = zeros(FullFloat, 4, 4)
    butcherA[2, 1] = 0.211324865405187
    butcherA[3, 1] = 0.709495523817170
    butcherA[3, 2] = -0.865314250619423
    butcherA[4, 1] = 0.705123240545107
    butcherA[4, 2] = 0.943370088535775
    butcherA[4, 3] = -0.859818194486069

    butcherAe = zeros(FullFloat, 4, 4)
    butcherAe[1, 1] = 0.788675134594813
    butcherAe[3, 1] = 0.051944240459852
    butcherAe[3, 3] = 0.788675134594813

    butcherB = zeros(FullFloat, 4)
    butcherB[2] = 0.5
    butcherB[4] = 0.5

    #jacobian doesn't change each time step so leave it out of the loop
    j1!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = vanderpol_novel_J!(J, y, ReduFloat(butcherAe[1, 1]*dt), α_r)
    j3!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = vanderpol_novel_J!(J, y, ReduFloat(butcherAe[3, 3]*dt), α_r)
    
    for (i, t) in enumerate(ts)
        #Make sure final time is reached correctly
        if i == nt 
            dt = tf - t_total
        end
        #Stage 1 (implicit)
        tmp_u = convert(Array{ReduFloat}, u)
        #implicit function changes with u, so redefine each time step
        tmp_uR = ReduFloat.(tmp_u)
        f1!(F::Array{ReduFloat}, y::Array{ReduFloat}) = vanderpol_novel_F!(F, y, tmp_uR, ReduFloat(butcherAe[1, 1]*dt), α_r)
        y1 = my_newtons!(f1!, j1!, tmp_uR, fn, jn)
        y1f = FullFloat.(y1)

        #Stage 2 (explicit)
        y2f = u[:] + (dt * butcherA[2, 1]) * rhs_f(y1f, α)

        #Stage 3 (implicit)
        tmp_u3 = zeros(FullFloat, 2)
        tmp_u3[:] = u[:] + (dt * butcherA[3, 1]) * rhs_f(y1f, α) 
        tmp_u3[:] += (dt * butcherA[3, 2]) * rhs_f(y2f, α)
        tmp_u3[:] += (dt * butcherAe[3, 1]) .* FullFloat.(rhs_r(y1, α_r))
        #implicit function changes with u, so redefine each time step
        tmp_u3R = ReduFloat.(tmp_u3)
        f3!(F::Array{ReduFloat}, y::Array{ReduFloat}) = vanderpol_novel_F!(F, y, tmp_u3R, ReduFloat(butcherAe[3, 3]*dt), α_r)
        y3 = my_newtons!(f3!, j3!, tmp_u3R, fn, jn)
        y3f = FullFloat.(y3)

        #Stage 4 (explicit)
        y4f = u[:] + (dt * butcherA[4, 1]) * rhs_f(y1f, α)
        y4f[:] += (dt * butcherA[4, 2]) * rhs_f(y2f, α)
        y4f[:] += (dt * butcherA[4, 3]) * rhs_f(y3f, α)
        
        #update stage
        unp1[:] = u[:] + (dt * butcherB[2]) * rhs_f(y2f, α)
        unp1[:] += (dt * butcherB[4]) * rhs_f(y4f, α)

        u[:] = unp1[:]
        t_total += dt 
    end
    
    return u
end

######### Main

#initial conditions
ti = FullFloat(0.0);
tf = FullFloat(1.0);

nt = parse(Int, ARGS[3]);
alpha = parse(FullFloat, ARGS[4]);

ts = collect(range(ti, tf, nt+1)[1:nt]);

u = novel_driver(nt, tf, alpha, ts)

global tot_elapsed = 0.0
nruns = 4
for k=1:nruns
    elapsed_time = @elapsed begin
        uel = novel_driver(nt, tf, alpha, ts)
    end;
    #println("One done in: $elapsed_time")
    global tot_elapsed = tot_elapsed + elapsed_time 
end
tot_elapsed = tot_elapsed / nruns
println("Ave elapsed time: $tot_elapsed")

#=
elapsed_time = @elapsed begin
    u = novel_driver(nt, tf, alpha, ts)
end;
println("Method Time: $elapsed_time")
=#

u_ref = zeros(Float128, 2)
open("./ref_sol/ref_sol_a$(ARGS[4]).txt") do f 
    for (i, line) in enumerate(eachline(f))
        u_ref[i] = parse(Float128, line)
    end
end

tmp = convert(Array{Float128}, u) - u_ref
l2_err = sqrt.(dot(tmp, tmp))
println("L2 error with reference: $l2_err")