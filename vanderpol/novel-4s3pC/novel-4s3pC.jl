
using Quadmath;
#using BenchmarkTools;
using LinearAlgebra;
using TimerOutputs;

##########################################
# Usage
#
# julia -t n_threads novel.jl FullType ReduType nt alpha
# Example
# julia -t 4 novel.jl Float64 Float32 100000 1.0
#
##########################################

#BLAS threading
BLAS.set_num_threads(Threads.nthreads())

const FullFloat = eval(Meta.parse(ARGS[1]));
const ReduFloat = eval(Meta.parse(ARGS[2]));

############ Functions

function vanderpol_novel_F!(fn::Array{ReduFloat}, y::Array{ReduFloat}, u::Array{ReduFloat}, dt::ReduFloat, alpha::ReduFloat)
    fn .= ReduFloat(0.0)

    fn[1] = u[1] + dt * (y[2]) - y[1]
    fn[2] = u[2] + dt * (alpha * (FullFloat(1.0) - y[1]*y[1]) * y[2] - y[1]) - y[2]

end

function vanderpol_novel_J!(J::Matrix{ReduFloat}, y::Array{ReduFloat}, dt::ReduFloat, alpha::ReduFloat)
    J[:, :] .= ReduFloat(0.0)

    J[1, 1] = ReduFloat(-1.0)
    J[1, 2] = dt
    J[2, 1] = dt * (alpha * y[1] * y[2] - ReduFloat(1.0))
    J[2, 2] = dt * (alpha * (ReduFloat(1.0) - y[1]*y[1])) - ReduFloat(1.0)

end

#=
My newtons method. Takes in inital guess x0 and mutates it to the solution over
max_iter iterations. Fn and Jn are preallocated placeholders for the iteration
computations. 
=#
function my_newtons!(f!, j!, x0::Array{ReduFloat}, Fn::Array{ReduFloat}, Jn::Matrix{ReduFloat})::Array{ReduFloat}
    tol = eps(ReduFloat) * .5e1; #Tolerance of the solver slightly larger than machine epsilon for that precision
    max_iter = 20; #maximum number of iterations for the solver 

    #println("starting newtons")
    for i=1:max_iter
        f!(Fn, x0)
        if norm(Fn) < tol 
            #println("iter: $i, tol: $(norm(Fn))")
            return x0
        end

        j!(Jn, x0)

        #x0 = x0 - inv(Jn) * Fn;
        x0 = x0 - Jn \ Fn;
        #println(norm(Fn))
    end
    return x0
end

function init_u()::Array{FullFloat}
    u = zeros(FullFloat, 2)
    u[1] = FullFloat(2.0)
    u[2] = FullFloat(0.0)
    return u
end

function rhs_f(y::Array{FullFloat}, alpha::FullFloat)::Array{FullFloat}
    u = zeros(FullFloat, 2)
    u[1] = y[2]
    u[2] = alpha * y[2] * (1 - y[1]*y[1]) - y[1]
    return u 
end

function rhs_r(y::Array{ReduFloat}, alpha::ReduFloat)::Array{ReduFloat}
    u = zeros(ReduFloat, 2)
    u[1] = y[2]
    u[2] = alpha * y[2] * (1 - y[1]*y[1]) - y[1]
    return u 
end

function novel_driver(nt::Integer, tf::FullFloat, alpha::FullFloat, ts::Array{FullFloat})::Array{FullFloat}
    dt = ts[2] - ts[1]
    t_total = FullFloat(ts[1])
    u = init_u()
    unp1 = copy(u)
    fn = zeros(ReduFloat, 2)
    jn = zeros(ReduFloat, 2, 2)
    alpha_r = ReduFloat(alpha)

    #Butcher coefficient matrix 
    butcherA = zeros(FullFloat, 4, 4)
    butcherA[2, 1] = -0.050470366527530
    butcherA[3, 1] = 0.368613367355336
    butcherA[3, 2] = 0.273504374252976
    butcherA[4, 1] = 1.803794668975043
    butcherA[4, 2] = 0.097485042980759
    butcherA[4, 3] = -1.895660952342050

    butcherAe = zeros(FullFloat, 4, 4)
    butcherAe[1, 1] = 0.511243008730995
    butcherAe[2, 1] = -1.999347282862640
    butcherAe[2, 2] = 1.957161067302390
    butcherAe[3, 1] = 0.443312893511937
    butcherAe[3, 2] = -0.573131033672219
    butcherAe[3, 3] = 0.128283796414019
    butcherAe[4, 1] = -2.0
    butcherAe[4, 2] = -0.160330320741428
    butcherAe[4, 3] = 0.579597314161362
    butcherAe[4, 4] = 1.484688928981990

    butcherB = zeros(FullFloat, 4)
    butcherB[1] = 0.002837446974069
    butcherB[2] = 0.336264433650450
    butcherB[3] = 0.806376720267787
    butcherB[4] = -0.145478600892306

    #jacobian doesn't change each time step so leave it out of the loop
    j1!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = vanderpol_novel_J!(J, y, ReduFloat(butcherAe[1, 1]*dt), alpha_r)
    j2!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = vanderpol_novel_J!(J, y, ReduFloat(butcherAe[2, 2]*dt), alpha_r)
    j3!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = vanderpol_novel_J!(J, y, ReduFloat(butcherAe[3, 3]*dt), alpha_r)
    j4!(J::Matrix{ReduFloat}, y::Array{ReduFloat}) = vanderpol_novel_J!(J, y, ReduFloat(butcherAe[4, 4]*dt), alpha_r)
    
    for (i, t) in enumerate(ts)
        #Make sure final time is reached correctly
        if i == nt 
            dt = tf - t_total
        end
        #Stage 1 (implicit)
        tmp_u = convert(Array{ReduFloat}, u)
        #implicit function changes with u, so redefine each time step
        tmp_uR = ReduFloat.(tmp_u)
        f1!(F::Array{ReduFloat}, y::Array{ReduFloat}) = vanderpol_novel_F!(F, y, tmp_uR, ReduFloat(butcherAe[1, 1]*dt), alpha_r)
        y1 = my_newtons!(f1!, j1!, tmp_uR, fn, jn)
        y1f = FullFloat.(y1)

        #Stage 2 (implicit)
        tmp_u2 = zeros(FullFloat, 2)
        tmp_u2[:] = u[:] + (dt * butcherA[2, 1]) .* rhs_f(y1f, alpha)
        tmp_u2[:] += (dt * butcherAe[2, 1]) .* FullFloat.(rhs_r(y1, alpha_r))
        #implicit function changes with u, so redefine each time step
        tmp_u2R = ReduFloat.(tmp_u2)
        f2!(F::Array{ReduFloat}, y::Array{ReduFloat}) = vanderpol_novel_F!(F, y, tmp_u2R, ReduFloat(butcherAe[2, 2]*dt), alpha_r)
        y2 = my_newtons!(f2!, j2!, tmp_u2R, fn, jn)
        y2f = FullFloat.(y2)

        #Stage 3 (implicit)
        tmp_u3 = zeros(FullFloat, 2)
        tmp_u3[:] = u[:] .+ (dt * butcherA[3, 1]) .* rhs_f(y1f, alpha) 
        tmp_u3[:] += (dt * butcherA[3, 2]) .* rhs_f(y2f, alpha)
        tmp_u3[:] += (dt * butcherAe[3, 1]) .* FullFloat.(rhs_r(y1, alpha_r))
        tmp_u3[:] += (dt * butcherAe[3, 2]) .* FullFloat.(rhs_r(y2, alpha_r))
        #implicit function changes with u, so redefine each time step
        tmp_u3R = ReduFloat.(tmp_u3)
        f3!(F::Array{ReduFloat}, y::Array{ReduFloat}) = vanderpol_novel_F!(F, y, tmp_u3R, ReduFloat(butcherAe[3, 3]*dt), alpha_r)
        y3 = my_newtons!(f3!, j3!, tmp_u3R, fn, jn)
        y3f = FullFloat.(y3)

        #Stage 4 (implicit)
        tmp_u4 = zeros(FullFloat, 2)
        tmp_u4[:] = u[:] .+ (dt * butcherA[4, 1]) .* rhs_f(y1f, alpha) 
        tmp_u4[:] += (dt * butcherA[4, 2]) .* rhs_f(y2f, alpha)
        tmp_u4[:] += (dt * butcherA[4, 3]) .* rhs_f(y3f, alpha)
        tmp_u4[:] += (dt * butcherAe[4, 1]) .* FullFloat.(rhs_r(y1, alpha_r))
        tmp_u4[:] += (dt * butcherAe[4, 2]) .* FullFloat.(rhs_r(y2, alpha_r))
        tmp_u4[:] += (dt * butcherAe[4, 3]) .* FullFloat.(rhs_r(y3, alpha_r))
        #implicit function changes with u, so redefine each time step
        tmp_u4R = ReduFloat.(tmp_u4)
        f4!(F::Array{ReduFloat}, y::Array{ReduFloat}) = vanderpol_novel_F!(F, y, tmp_u4R, ReduFloat(butcherAe[4, 4]*dt), alpha_r)
        y4 = my_newtons!(f4!, j4!, tmp_u4R, fn, jn)
        y4f = FullFloat.(y4)
        
        #update stage
        unp1[:] = u[:] + (dt * butcherB[1]) * rhs_f(y1f, alpha)
        unp1[:] += (dt * butcherB[2]) * rhs_f(y2f, alpha)
        unp1[:] += (dt * butcherB[3]) * rhs_f(y3f, alpha)
        unp1[:] += (dt * butcherB[4]) * rhs_f(y4f, alpha)

        u[:] = unp1[:]
        t_total += dt 
    end
    
    return u
end

######### Main

#initial conditions
ti = FullFloat(0.0);
tf = FullFloat(1.0);

nt = parse(Int, ARGS[3]);
alpha = parse(FullFloat, ARGS[4]);

ts = collect(range(ti, tf, nt+1)[1:nt]);

u = novel_driver(nt, tf, alpha, ts)

global tot_elapsed = 0.0
nruns = 4
for k=1:nruns
    elapsed_time = @elapsed begin
        uel = novel_driver(nt, tf, alpha, ts)
    end;
    #println("One done in: $elapsed_time")
    global tot_elapsed = tot_elapsed + elapsed_time 
end
tot_elapsed = tot_elapsed / nruns
println("Ave elapsed time: $tot_elapsed")

#=
elapsed_time = @elapsed begin
    u = novel_driver(nt, tf, alpha, ts)
end;
println("Method Time: $elapsed_time")
=#

u_ref = zeros(Float128, 2)
open("./ref_sol/ref_sol_a$(ARGS[4]).txt") do f 
    for (i, line) in enumerate(eachline(f))
        u_ref[i] = parse(Float128, line)
    end
end

tmp = convert(Array{Float128}, u) - u_ref
l2_err = sqrt.(dot(tmp, tmp))
println("L2 error with reference: $l2_err")