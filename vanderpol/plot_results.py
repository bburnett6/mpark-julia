
import itertools
import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib.cm as cm
import argparse as ap 
import os 

def plot_for(method_name, ncorr, nthreads, alpha, is_save, save_type="png"):
	output_basedir = f"output_threads-{nthreads}/{method_name}/corr-{ncorr}/"
	nocorr_basedir = f"output_threads-{nthreads}/{method_name}/corr-0/"
	if not os.path.exists(f'{output_basedir}'):
		print('Outputs dont exist. Check args?')
		return

	#Get only the types that are available in the filenames.
	#getFloatType is used for sorting on the integer value of the float type (ie Float128 > Float64)
	getFloatType = lambda s: int(s.split('Float')[-1])
	#types is the set of all available types
	types = list(set(map(lambda s: s.split('_')[1], os.listdir(output_basedir))))
	#sort the types in reverse order based on the getFloatType function
	types.sort(reverse=True, key=getFloatType)
	#get all type combinations
	type_combos = list(itertools.combinations_with_replacement(types, 2))
	#similar process with getting the number of timesteps used
	nts = list(set(map(lambda s: int(s.split('.')[0].split('-')[-1]), os.listdir(output_basedir))))
	nts.sort()

	alphas = ['0.001', '0.01', '0.1', '1.0', '10.0', '100.0', '1000.0', '100000000.0']
	a_line_index = 2*alphas.index(alpha)

	runs = []
	for d_full, d_redu in type_combos:
		run = {}
		run['full'] = d_full
		run['redu'] = d_redu
		run['dts'] = []
		run['runtimes'] = []
		run['errors'] = []
		for nt in nts:
			run['dts'].append(1/10**nt)
			filename = f'out_{d_full}_{d_redu}_nt-{nt}.txt'
			with open(os.path.join(output_basedir, filename), 'r') as f:
				#print(filename)
				output = f.read()
				#print(output)
				lines = output.splitlines()
				run['runtimes'].append(float(lines[a_line_index].split()[-1]))
				run['errors'].append(float(lines[a_line_index+1].split()[-1]))
		runs.append(run)

	runs_nocorr = []
	for d_full, d_redu in type_combos:
		run = {}
		run['full'] = d_full
		run['redu'] = d_redu
		run['dts'] = []
		run['runtimes'] = []
		run['errors'] = []
		for nt in nts:
			run['dts'].append(1/10**nt)
			filename = f'out_{d_full}_{d_redu}_nt-{nt}.txt'
			with open(os.path.join(output_basedir, filename), 'r') as f:
				output = f.read()
				lines = output.splitlines()
				run['runtimes'].append(float(lines[0].split()[-1]))
				run['errors'].append(float(lines[1].split()[-1]))
		runs_nocorr.append(run)

	if is_save:
		if not os.path.exists(f'./images/{method_name}/corrections{ncorr}/alpha{alpha}'):
			os.makedirs(f'./images/{method_name}/corrections{ncorr}/alpha{alpha}')

	#Convergence
	colors = cm.turbo(np.linspace(0, 1, len(runs)))
	for i, run in enumerate(runs):
		if run['full'] == run['redu']:
			plt.loglog(runs_nocorr[i]['dts'], runs_nocorr[i]['errors'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(runs_nocorr[i]['dts'], runs_nocorr[i]['errors'], color=colors[i], s=(len(runs)-i)*20)
		else:
			plt.loglog(run['dts'], run['errors'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(run['dts'], run['errors'], color=colors[i], s=(len(runs)-i)*20)
	plt.xlabel('dt')
	plt.ylabel('error')
	#plt.ylim(top=1e-3)
	plt.xticks(run['dts'])
	plt.grid()
	#plt.ylim(top=1e0)
	plt.legend(prop={'size': 8})
	if not is_save:
		plt.title(f'{method_name.upper()} Corrections={ncorr} Alpha={alpha}')
		plt.show()
	else:
		plt.savefig(f'./images/{method_name}/corrections{ncorr}/alpha{alpha}/err_v_dt.{save_type}', format=f'{save_type}', dpi=1000)
		plt.clf()

	#Efficiency
	colors = cm.turbo(np.linspace(0, 1, len(runs)))
	for i, run in enumerate(runs):
		if run['full'] == run['redu']:
			plt.loglog(runs_nocorr[i]['runtimes'], runs_nocorr[i]['errors'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(runs_nocorr[i]['runtimes'], runs_nocorr[i]['errors'], color=colors[i], s=(len(runs)-i)*20)
		else:
			plt.loglog(run['runtimes'], run['errors'], label=f'F:{run["full"]},R:{run["redu"]}', color=colors[i])
			plt.scatter(run['runtimes'], run['errors'], color=colors[i], s=(len(runs)-i)*20)
	plt.xlabel('runtime (s)')
	plt.ylabel('error')
	#plt.ylim(top=1e-3)
	#plt.xticks(run['dts'])
	plt.grid()
	#plt.ylim(top=1e0)
	plt.legend(prop={'size': 8})
	if not is_save:
		plt.title(f'{method_name.upper()} Corrections={ncorr} Alpha={alpha}')
		plt.show()
	else:
		plt.savefig(f'./images/{method_name}/corrections{ncorr}/alpha{alpha}/err_v_runtime.{save_type}', format=f'{save_type}', dpi=1000)
		plt.clf()

if __name__ == '__main__':
	parser = ap.ArgumentParser(description='Plot the output of runs of the Vanderpol equation for a given number of corrections')
	parser.add_argument(
		'--ncorr',
		dest='ncorr',
		default='0',
		help='Number of corrections'
		)
	parser.add_argument(
		'-t', 
		dest='threads', 
		default="8",
		help='Number of threads used by BLAS'
		)
	parser.add_argument(
		'-m', 
		dest='method', 
		default="impmid",
		help='Name of the method used to compute solution'
		)
	parser.add_argument(
		'-a',
		dest='alpha',
		default="1.0",
		help="The stiffness coefficient alpha used. range: [0.001..1000.0] by powers of 10 (and 10**8)"
		)
	parser.add_argument(
		'-s',
		dest='is_save',
		action='store_true',
		help='Save files'
		)
	args = parser.parse_args()

	plot_for(args.method, args.ncorr, args.threads, args.alpha, args.is_save)