
# Mixed Precision Additive Runge-Kutta (MPARK) Methods in Julia

This repository contains codes implementing the MPARK methods in the Julia language. These codes were designed to run a benchmarking experiment on the Carnie HPC cluster in order to evaluate the efficiency benefits provided by the methods. The following tests were used to perform this experiment: the Van der Pol equation, 1/2 dimensional heat equation, and the 1 dimensional Burger's equation. Each test can be found in their respective directories and can be treated as stand-alone tests.

## Dependencies

### Julia

To run the Julia codes the following packages are needed:

* Quadmath (x86-64 only, comment out this include if running on ARMv8)
* LinearAlgebra
* TimerOutputs

### Python

To run the python experiment reproduction scripts the following libraries are needed:

* Numpy
* Matplotlib
* Jinja2
* Argparse

## Running an individual test

Each of the tests are stand-alone experiments and follow a similar workflow. To start running, a reference solution must first be generated. The following illustrates creating a reference solution for the Burger's equation using 10^7 timesteps (dt=10^-7) for 50 spacial gridpoints:

```
cd burgers/ref_sol
julia rk4.jl Float128 Float128 10000000 50 1
```

To verify the reference solution is working, a simple convergence test can be performed with

```
julia rk4.jl Float128 Float128 100 50 0
julia rk4.jl Float128 Float128 1000 50 0
```

With the reference solution now generated we can run an individual method's sample convergence test. The following illustrates testing the implicit midpoint rule for 1000/10000 timesteps using that will use the reference solution we just generated for 50 space points using 4 threads for BLAS:

```
cd ..
julia -t 4 impmid/impmid.jl Float64 Float64 1000 50
julia -t 4 impmid/impmid.jl Float64 Float64 10000 50
```

Testing the naive mixed precision capabilities is possible by changing the `FloatXXX` arguments with the first being the target or full precision, and the second being the reduced precision used for the implicit sections of the methods computation. The following precisions have been tested: `Float16` (done in software on all architectures. ARMv8 is a WIP), `Float32`, `Float64`, `Float128` (On x86-64 uses Quadmath 128 bit in software, on ARMv8 uses 80 bit in software), `BigFloat` (Julia's arbitrary precision). 

To run the corrected methods, a similar run is performed with the addition of another argument specifying the number of correction steps

```
julia -t 4 impmid/impmid-corrections.jl Float64 Float16 1000 50 2
julia -t 4 impmid/impmid-corrections.jl Float64 Float16 10000 50 2
```

## Running the performance experiment

The performance experiment is orchistrated using python scripts. These scripts are designed to be run on the Carnie HPC Cluster in order to parallelize the collection of data. On Carnie a virtual or conda environment with the libraries outlined in the dependencies section is required. Here is an example of running a full experiment for the implicit midpoint rule:

```
cd mpark-julia/burgers
python run_batches.py --nx 50 --ncorr 0 -t 24 -m impmid
python run_batches.py --nx 50 --ncorr 1 -t 24 -m impmid
python run_batches.py --nx 50 --ncorr 2 -t 24 -m impmid
```

This above example will spawn 30 jobs making use of an entire node (par some exceptions with quad/half precision) using the slurm batch scheduler fanning out the evaluation of the experiment for permutations of the precisions `['Float128', 'Float64', 'Float32', 'Float16']` in an array job format, where each array iteration will run for a different `dt=10^-n` for `n=[2..6]`. 

The results of these runs will all be placed into an output directory with the following structure:

```
output_mpark/{method_name}/corr-{ncorr}/nx{nx}/err_{fulltype}_{redutype}_nt-%a.txt
```

These can then be plotted once transferred from the cluster using:

```
python plot_results.py --nx 50 --ncorr 1 -t 24 -m impmid
```

Or a Latex document with everything can be generated with

```
python gen_latex.py --method impmid --outdir output_mpark
```

## Known issues

* Currently the 1/2D heat equation tests don't share the above structure. Copying and tweeking the `run_batches.py` and `plot_results.py` and fixing the internal final time stepping is needed.

* `Float128` and `Float16` do not use all available threads. Although Julia will gladly allocate and use all available cores on a node, the are not strictly necessary for using these precisions. Adding an if statement in the `run_batches.py` for this somewhere should help. *currently fixed in burgers*

* `Float128` and `BigFloat` take a LOOOOOOONG time to process at for small dt. Averaging of the runtime is used in the Julia scripts, but are really only useful for when the codes will be impacted by system noise for large dt. It would likely be alright for a small dt to therefore only run once rather than multiple times for an average runtime for these precisions. *currently fixed in burgers*
