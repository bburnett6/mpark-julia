using Quadmath;
using LinearAlgebra;
using TimerOutputs;
using BenchmarkTools;

##########################################
# Usage
#
# julia impmid.jl FullType ReduType nt nx ncorr
# Example
# julia impmid.jl Float64 Float32 100000 20 1
#
##########################################

#BLAS threading
BLAS.set_num_threads(Threads.nthreads())

# Types for mixed precision using type aliasing
const fullFloat = eval(Meta.parse(ARGS[1]));
const reduFloat = eval(Meta.parse(ARGS[2]));

# Initialization functions

function init_a(n::Integer, gamma::fullFloat)::Matrix{fullFloat}
    a = Matrix{fullFloat}(I, n*n, n*n)
    for i=n+1:n*(n-1)
        if ((mod(i, n) != 0) & (mod(i, n) != 1))
            a[i, i+1] = gamma
            a[i, i] = fullFloat(-4.0) * gamma
            a[i, i-1] = gamma
            a[i, i-n] = gamma
            a[i, i+nx] = gamma
        end
    end
    
    return a
end

function init_b(a::Matrix{fullFloat}, n::Integer, gamma::fullFloat)::Matrix{reduFloat}
    b = Matrix{reduFloat}(I, n*n, n*n)
    b = b - convert(Matrix{reduFloat}, a) / reduFloat(2.0)
    return b 
end

function init_u(n::Integer)::Matrix{fullFloat}
    u = zeros(fullFloat, n, n)
    for i=2:n-1
        for j=2:n-1
            u[i, j] = sin(fullFloat(i)/fullFloat(n-1) * pi) * sin(fullFloat(j)/fullFloat(n-1) * pi)
        end
    end
    
    return u
end

function impmid_driver(nt::Integer, nx::Integer, ts::Array{fullFloat}, xs::Array{fullFloat}, beta::fullFloat, ncorr::Integer)::Array{fullFloat}
    dt = ts[2] - ts[1]
    dx = xs[2] - xs[1]
    gamma = beta * dt / (dx * dx)
    u = reshape(init_u(nx), :, 1) #u is an array
    unp1 = u
    a = init_a(nx, gamma)
    b = init_b(a, nx, gamma)
    #see impmid-corrections_LU for a more performant version
    
    for (i, t) in enumerate(ts)
        #implicit step
        y1 = b\convert(Array{reduFloat}, u)
        
        #correction step
        yk = convert(Array{fullFloat}, y1)
        for p=1:ncorr
            yk = u + 0.5 * a * yk
        end

        #explicit step
        u = u + a * yk
    end
    
    return u
end

####### Main

beta = fullFloat(1.0);
ti = fullFloat(0.0);
tf = fullFloat(0.1);
xi = fullFloat(0.0);
xf = fullFloat(1.0);

nt = parse(Int, ARGS[3]);
nx = parse(Int, ARGS[4]);
ncorr = parse(Int, ARGS[5]);

ts = collect(range(ti, tf, nt+1)[1:nt]); #Do not include end point! Use slicing achieve this because: https://github.com/JuliaLang/julia/issues/27097
xs = collect(range(xi, xf, nx)); # do include endpoint

#global tot_elapsed = 0.0
#nruns = 5
#for k=1:nruns
#    elapsed_time = @elapsed begin
#        u = impmid_driver(nt, nx, ts, xs, beta)
#    end;
#    global tot_elapsed = tot_elapsed + elapsed_time 
#end
#tot_elapsed = tot_elapsed / nruns
#println("Ave elapsed time: $tot_elapsed")

#elapsed_time = @elapsed begin
#    u = impmid_driver(nt, nx, ts, xs, beta)
#end;
#println("Method time: $elapsed_time")
BenchmarkTools.DEFAULT_PARAMETERS.samples = 4;
@btime impmid_driver(nt, nx, ts, xs, beta, ncorr)

u = impmid_driver(nt, nx, ts, xs, beta, ncorr);

u_ref = zeros(Float128, nx, nx)
open("./ref_sol/ref_sol_$nx.txt") do f 
    for (i, line) in enumerate(eachline(f))
        #println(length(split(line, ',')))
        for (j, val) in enumerate(split(line, ','))
            #print("$val ")
            u_ref[i, j] = parse(fullFloat, val)
        end
    end
end

dx = Float128(xs[2] - xs[1])
tmp = convert(Array{Float128}, reshape(u, nx, nx)) - u_ref
l2_err = sqrt.(dx * dot(tmp, tmp))
println("L2 error with reference: $l2_err")