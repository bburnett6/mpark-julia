using Quadmath;
using LinearAlgebra;
using DelimitedFiles;
using TimerOutputs;
using Plots;

##########################################
# Usage
#
# julia rk4.jl FullType ReduType nt nx write_ref
# Example:
# julia rk4.jl Float128 Float128 1000000 100 1
#
##########################################

# Types for mixed precision using type aliasing
const fullFloat = eval(Meta.parse(ARGS[1]));
const reduFloat = eval(Meta.parse(ARGS[2]));

# Initialization functions

function init_u(n::Integer)::Matrix{fullFloat}
    u = zeros(fullFloat, n, n)
    for i=2:n-1
        for j=2:n-1
            u[i, j] = sin(fullFloat(i)/fullFloat(n-1) * pi) * sin(fullFloat(j)/fullFloat(n-1) * pi)
        end
    end
    
    return u
end

#function stencel(x::Matrix{fullFloat}, p::Integer, q::Integer)::fullFloat
#    return fullFloat(-4.0)*x[p, q] + x[p-1, q] + x[p+1, q] + x[p, q-1] + x[p, q+1]
#end

function fi(u::Array{fullFloat}, n::Integer, t::fullFloat, dx::fullFloat, beta::fullFloat)::Matrix{fullFloat}
    fn = zeros(fullFloat, n, n)

    for i=2:n-1 
        for j=2:n-1
            stencel = fullFloat(-4.0)*u[i, j] + u[i-1, j] + u[i+1, j] + u[i, j-1] + u[i, j+1]
            fn[i, j] = beta/(dx*dx) * stencel
            #fn[i, j] = beta/(dx*dx) * stencel(u, i, j)
        end
    end 

    return fn 
end

function rk4_driver(nt::Integer, nx::Integer, ts::Array{fullFloat}, xs::Array{fullFloat}, beta::fullFloat)::Matrix{fullFloat}
    dt = ts[2] - ts[1]
    dx = xs[2] - xs[1]
    gamma = beta * dt / (dx * dx)
    u = init_u(nx)
    
    anim = Animation()
    plt = heatmap(u, clim=(0, 1.0))
    frame(anim, plt)
    for (i, t) in enumerate(ts)
        k1 = fi(u, nx, t, dx, beta)
        tmp = u + dt/fullFloat(2.0) * k1 
        k2 = fi(tmp, nx, t + dt/fullFloat(2.0), dx, beta)
        tmp = u + dt/fullFloat(2.0) * k2 
        k3 = fi(tmp, nx, t + dt/fullFloat(2.0), dx, beta)
        tmp = u + dt * k3 
        k4 = fi(tmp, nx, t + dt, dx, beta)

        u = u + fullFloat(1.0)/fullFloat(6.0) * dt * (k1 + fullFloat(2.0)*k2 + fullFloat(2.0)*k3 + k4)
        if mod(i, 10) == 0
            plt = heatmap(reshape(u, nx, nx), clim=(0, 1.0))
            frame(anim, plt)
        end
    end
    gif(anim, "rk4_$nx.gif", fps=15)
    
    return u
end

####### Main

beta = fullFloat(1.0);
ti = fullFloat(0.0);
tf = fullFloat(0.1);
xi = fullFloat(0.0);
xf = fullFloat(1.0);

nt = parse(Int, ARGS[3]);
nx = parse(Int, ARGS[4]);
write_ref = parse(Int, ARGS[5]);

ts = collect(range(ti, tf, nt+1)[1:nt]); #Do not include end point! Use slicing achieve this because: https://github.com/JuliaLang/julia/issues/27097
xs = collect(range(xi, xf, nx)); # do include endpoint

elapsed_time = @elapsed begin
    u = rk4_driver(nt, nx, ts, xs, beta)
end;

println("Method time: $elapsed_time")

if write_ref == 1
    open("ref_sol_$nx.txt", "w") do io 
        writedlm(io, u, ',')
    end 
end 

#This is broken. Tryparse fails and is unable to read float128
#u_ref = readdlm("ref_sol_$nx.txt", ',', fullFloat, '\n')
u_ref = zeros(fullFloat, nx, nx)
open("ref_sol_$nx.txt") do f 
    for (i, line) in enumerate(eachline(f))
        #println(length(split(line, ',')))
        for (j, val) in enumerate(split(line, ','))
            #print("$val ")
            u_ref[i, j] = parse(fullFloat, val)
        end
    end
end

dx = xs[2] - xs[1]
tmp = u - u_ref
l2_err = sqrt.(dx * dot(tmp, tmp))
println("L2 error with reference: $l2_err")